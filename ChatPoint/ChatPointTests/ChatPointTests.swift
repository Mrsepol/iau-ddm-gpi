//
//  ChatPointTests.swift
//  ChatPointTests
//
//  Created by Dany Mota on 02/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import XCTest
import Firebase


@testable import ChatPoint

class ChatPointTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
/*
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
*/
//    func testChatCreation() {
//        let chat = Chat(id: "T##String", img: "wdw", lat: 2.21, lng: 2.21, name: "wdwe", radius: 12)
//    }
//    
//    func testAppManagerCreation() {
//        AppManager.init()
//    }
//    
    func testAppManagerInstances() {
        XCTAssert(AppManager.instance.alertManager != nil)
        XCTAssert(AppManager.instance.apiHandler != nil)
        XCTAssert(AppManager.instance.inputManager != nil)
    }
    
    func testParseValidChat() {
        var dict = [String: Any]()
        dict["location"] = GeoPoint(latitude: 39.731465, longitude: -8.826234)
        dict["name"] = "Chat válido"
        dict["radius"] = 1000
        dict["hide"] = true
        dict["created_at"] = 2.2
        dict["expire_date"] = Timestamp.init(date: Date())
        dict["img"] = ImageHelper.getBase64FromImage(image: UIImage(named:"enterChat")!)
        
        let validChat = try! Chat(id: "wdwdwdwdw", img: dict["img"] as! String, location: dict["location"] as! GeoPoint, name: dict["name"] as! String, radius: dict["radius"] as! Int, hide: dict["hide"] as! Bool, created_at: dict["created_at"] as! Double, expire_date: dict["expire_date"] as! Timestamp)
    }
    
    func testParseInvalidChatRange() {
        var dict = [String: Any]()
        dict["location"] = GeoPoint(latitude: 39.731465, longitude: -8.826234)
        dict["name"] = "Chat válido"
        dict["radius"] = -1
        dict["hide"] = true
        dict["created_at"] = 2.2
        dict["expire_date"] = Timestamp.init(date: Date())
        dict["img"] = ImageHelper.getBase64FromImage(image: UIImage(named:"enterChat")!)
        do {
            let invalidChat = try Chat(id: "wdwdwdwdw", img: dict["img"] as! String, location: dict["location"] as! GeoPoint, name: dict["name"] as! String, radius: dict["radius"] as! Int, hide: dict["hide"] as! Bool, created_at: dict["created_at"] as! Double, expire_date: dict["expire_date"] as! Timestamp)
            XCTFail()
        } catch {
        
        }
    }

    func testCreateValidChatPointUser() {
        let user = ChatPointUser(uId: "abcsd", email: "test@test.com", name: "test")
    }
}
