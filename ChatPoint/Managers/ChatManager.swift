//
//  ChatManager.swift
//  ChatPoint
//
//  Created by Alexandre Frazao on 28/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import Foundation
import Firebase

// TODO: Implementar um protocolo para "ouvir" as respostas, ou entao passar callbacks
// Update: Ver se esta a funcionar
class ChatManager {
    var myChats = [Chat]()
    var nearbyChats = [Chat]()
    
    static let shared = ChatManager()
    
    private init() {}
    
    public func updateChats(lat: Double, lng: Double, range: Double, completionHandler: ((ResponseCodes?) -> Void)?) -> Void {
        // Mais ou menos 1km
        let staticLat = 0.008983
        let staticLng = 0.015060
    
        let lowerLat = lat - (staticLat * range)
        let lowerLon = lng - (staticLng * range)
        
        let greaterLat = lat + (staticLat * range)
        let greaterLon = lng + (staticLng * range)
  
        let lesserGeopoint = GeoPoint(latitude: lowerLat, longitude: lowerLon)
        let greaterGeopoint = GeoPoint(latitude: greaterLat, longitude: greaterLon)
        
        let docRef = Firestore.firestore().collection("chats")
        let query = docRef.whereField("location", isGreaterThan: lesserGeopoint).whereField("location", isLessThan: greaterGeopoint)

        query.getDocuments { snapshot, error in
            if let error = error {
                print("Error getting documents: \(error)")
                if let handler = completionHandler {
                    handler(ResponseCodes.serverError)
                }
                
            } else {
                self.parseData(docs: snapshot!.documents)
                if let handler = completionHandler {
                    handler(ResponseCodes.success)
                }
            }
        }
    }
    
    func parseData(docs: [QueryDocumentSnapshot]) {
        nearbyChats.removeAll()
        for doc in docs {
            let docData = doc.data()
            
            let chatLocation = docData["location"] as! GeoPoint
            
            let newChat = Chat(
                id: doc.documentID,
                img: docData["img"] as! String,
                lat: chatLocation.latitude,
                lng: chatLocation.longitude,
                name: docData["name"] as! String,
                radius: docData["radius"] as! Int
            )
            
            nearbyChats.append(newChat)
        }
    }
    
    func searchInNearbyChats(name: String) -> [Chat]? {
        let result = nearbyChats.filter({( chat : Chat) -> Bool in
            return chat.name.lowercased().contains(name.lowercased())
        })
        
        return result
    }
    
    func searchInMyChats(name: String) -> [Chat]? {
        let result = myChats.filter({( chat : Chat) -> Bool in
            return chat.name.lowercased().contains(name.lowercased())
        })
        
        return result
    }
    
    func getNearbyChatByID(id: String) -> Chat? {
        return nearbyChats.first(where: { (chat: Chat) -> Bool in
            return chat.id == id
        })
    }
}
