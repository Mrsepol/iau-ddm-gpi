//
//  MyChatListTableViewCell.swift
//  ChatPoint
//
//  Created by Dany Mota on 08/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit

class MyChatListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var chatDistance: UILabel!
    @IBOutlet weak var chatName: UILabel!
    @IBOutlet weak var chatImage: UIImageView!
    
    var chatID: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
