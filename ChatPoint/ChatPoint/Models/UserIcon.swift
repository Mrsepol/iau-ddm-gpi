//
//  UserIcon.swift
//  ChatPoint
//
//  Created by Joao Marques on 29/12/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import Foundation
import UIKit

class UserIcon:  NSObject, NSSecureCoding {
    
    var userID: String
    var icon: UIImage
    //Date to verify if the image should be refreshed or not
    var timestamp: Date
    
    static var supportsSecureCoding: Bool {
        return true
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(userID, forKey: Constants.USERICON_USERID)
        aCoder.encode(ImageHelper.getBase64FromImage(image: icon), forKey: Constants.USERICON_ICON)
        aCoder.encode(timestamp.timeIntervalSince1970, forKey: Constants.USERICON_TIMESTAMP)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.userID = aDecoder.decodeObject(forKey: Constants.USERICON_USERID) as! String
        self.timestamp = Date(timeIntervalSince1970: aDecoder.decodeDouble(forKey: Constants.USERICON_TIMESTAMP))
        do{
            self.icon = try ImageHelper.getImageFromBase64String(string: aDecoder.decodeObject(forKey: Constants.USERICON_ICON) as! String)
        } catch {
            self.icon = UIImage(named: "default_profile")!
        }
    }
    
    
    init(userID: String, icon: UIImage) {
        self.icon = icon
        self.userID = userID
        self.timestamp = Date()
    }
    
    init(userID: String, profilePhoto: UIImage) {
        self.icon = ImageHelper.resizeImage(image: profilePhoto, targetSize: CGSize(width: 32, height: 32))
        self.userID = userID
        self.timestamp = Date()
    }
    
    init(userID: String, profilePhoto: String) {
        do{
            self.icon = ImageHelper.resizeImage(image: try ImageHelper.getImageFromBase64String(string: profilePhoto), targetSize: CGSize(width: 32, height: 32))
        } catch {
            self.icon = UIImage(named: "default_profile")!
        }
        self.userID = userID
        self.timestamp = Date()
    }
    
    
    func toArray() -> [String:Any]{
        var data = [String:Any]()
        
        data["icon"] = ImageHelper.getBase64FromImage(image: icon)
        return data
    }
    
}

