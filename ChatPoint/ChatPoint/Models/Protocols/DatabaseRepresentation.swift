//
//  DatabaseRepresentation.swift
//  ChatPoint
//
//  Created by Hugo freire on 16/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import Foundation

protocol DatabaseRepresentation {
    var representation: [String: Any] { get }
}
