//
//  AppManager.swift
//  ChatPoint
//
//  Created by João Pereira Marques on 22/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import Network

class AppManager {
    
    static let instance = AppManager()
    var apiHandler : APIHandler
    var inputManager : InputManager
    var chatManager : ChatManager
    let defaults = UserDefaults()
    var user : ChatPointUser?
    var alertManager : AlertManager
    var userIconManager: UserIconManager
    var lastKnowLocation: CLLocationCoordinate2D?
    var selectedChat: Chat?
    let networkMonitor = NWPathMonitor()
    var hasConnection: Bool = false
    
    private init() {
        self.apiHandler = APIHandler()
        self.chatManager = ChatManager()
        self.inputManager = InputManager()
        self.alertManager = AlertManager()
        self.userIconManager = UserIconManager()
        
        let queue = DispatchQueue(label: "Monitor")
        networkMonitor.start(queue: queue)
        networkMonitor.pathUpdateHandler = { path in
            self.hasConnection = path.status == .satisfied
            print("Network status changed: \(self.hasConnection)")
        }
    }
    
    //Remover
    func setUser(uId:String, email:String, name:String){
        self.user = ChatPointUser(uId: uId, email:email, name:name)
        self.persistUser()
    }
    
    func setUser(user:ChatPointUser){
        self.user = user
        self.persistUser()
    }
    
    func clearUser() {
        self.user = nil
    }
    
    private func parseUserToData(user: ChatPointUser) -> Data?{
        return try?
            NSKeyedArchiver.archivedData(withRootObject: user, requiringSecureCoding: false)
    }
    
    private func parseDataToUser(unarchivedObject: Data) -> ChatPointUser? {
        return try? NSKeyedUnarchiver.unarchivedObject(ofClasses: [ChatPointUser.self], from: unarchivedObject) as! ChatPointUser
    }
    
    func getUser() -> ChatPointUser? {
        return self.user
    }
    
    func loadUser () {
        if(!self.apiHandler.isLoggedIn()){
            self.user = nil
            return
        }
        if let d = defaults.data(forKey: Constants.USER_ARCHIVE) {
            self.user = self.parseDataToUser(unarchivedObject: d)
        }
    }
    
    func persistUser() {
        if let user = self.user {
            let data = self.parseUserToData(user: user)
            
            if data != nil {
                self.defaults.set(data, forKey: Constants.USER_ARCHIVE)
                self.defaults.synchronize()
            }
        }
    }
    
    func makeUserLogout(controller: UIViewController, showAlertOnSucess: Bool) -> Void {
        
        AppManager.instance.apiHandler.signOut { (responseCode) in

            if responseCode!.getType() == "success" {
                if showAlertOnSucess{
                    let alertController = UIAlertController(title: "Success", message: responseCode!.getMessage(), preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert) in
                        DispatchQueue.main.async {
                            if let mapController = controller.storyboard?.instantiateViewController(
                                withIdentifier: "Tab") {
                                controller.present(mapController, animated: false, completion: nil)
                            }
                        }
                    })

                    alertController.addAction(defaultAction)
                    DispatchQueue.main.async {
                        controller.present(alertController, animated: true, completion: nil)
                    }
                }
                
                self.defaults.removeObject(forKey: Constants.USER_ARCHIVE)
                self.defaults.removeObject(forKey: Constants.CHATS_ARCHIVE)
                
                return
            }

            let alertController = UIAlertController(title: "Error", message: responseCode!.getMessage(), preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            DispatchQueue.main.async {
                controller.present(alertController, animated: true, completion: nil)
            }

            return
        }
        return
    }
}
