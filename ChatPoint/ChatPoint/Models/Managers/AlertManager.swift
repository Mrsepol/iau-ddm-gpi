//
//  ErrorMessageController.swift
//  ChatPoint
//
//  Created by Alexandre Frazao on 17/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import Foundation
import UIKit

class AlertManager{
    
    static func showErrorAlert(controller: UIViewController, _ message:String, title: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        
        DispatchQueue.main.async {
            controller.present(alertController, animated: true, completion: nil)
        }
    }
}
