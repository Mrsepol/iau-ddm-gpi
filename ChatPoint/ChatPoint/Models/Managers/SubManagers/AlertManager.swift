//
//  ErrorMessageController.swift
//  ChatPoint
//
//  Created by Alexandre Frazao on 17/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import Foundation
import UIKit

class AlertManager{
    
    func showErrorAlert(controller: UIViewController, _ message:String, title: String, onOk: ((UIAlertAction) -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: onOk)
        alertController.addAction(defaultAction)
        
        DispatchQueue.main.async {
            controller.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showLoadingAlert(controller: UIViewController, _ message:String){
        var message = message
        if(!AppManager.instance.inputManager.validateEmptyString(string: message)){
            message = "Please wait..."
        }
        
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        DispatchQueue.main.async {
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
    func hiddenLoadingAlert(controller: UIViewController, completion: (() -> Void)?){
        DispatchQueue.main.async {
            controller.dismiss(animated: false, completion: completion)
        }
    }
    // TODO: Complete this
    func alertToSettings(controller: UIViewController,title: String, message: String) {
        let alertController = UIAlertController (title: title, message: message, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Stay", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
}
