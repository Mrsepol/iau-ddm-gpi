//
//  ChatManager.swift
//  ChatPoint
//
//  Created by Alexandre Frazao on 28/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import Foundation
import Firebase
import CoreLocation
import FirebaseFirestore

class ChatManager {
    var myChats = [Chat]()
    var nearbyChats = [Chat]()
    let defaults = UserDefaults()
    
    init() {
    }
    
    public func updateChats(lat: Double, lng: Double, range: Double, completionHandler: ((ResponseCodes?) -> Void)?) -> Void {
        // Mais ou menos 1km
        let staticLat = 0.008983
        let staticLng = 0.015060
        
        let lowerLat = lat - (staticLat * range)
        let lowerLon = lng - (staticLng * range)
        
        let greaterLat = lat + (staticLat * range)
        let greaterLon = lng + (staticLng * range)
        // esquerda
        let lesserGeopoint = GeoPoint(latitude: lowerLat, longitude: lowerLon)
        // direita
        let greaterGeopoint = GeoPoint(latitude: greaterLat, longitude: greaterLon)
        
        let docRef = Firestore.firestore().collection("chats")
        let query = docRef
            .order(by: "location", descending: false)
            .order(by: "created_at", descending: true)
            .whereField("location", isGreaterThan: lesserGeopoint)
            .whereField("location", isLessThan: greaterGeopoint)
        
        query.getDocuments { snapshot, error in
            if let error = error {
                print("Error getting documents: \(error)")
                if let handler = completionHandler {
                    handler(ResponseCodes.serverError)
                }
                
            } else {
                
                self.nearbyChats = self.parseChats(docs: snapshot!.documents)
                
                if let handler = completionHandler {
                    handler(ResponseCodes.success)
                }
            }
        }
    }
    
    func updateChat(chat: Chat, completionHandler: ((ResponseCodes?) -> Void)?){
        
        if let index = self.getMyChatIndexByID(id: chat.id){
            myChats.remove(at: index)
            myChats.insert(chat, at: index)
            
            self.persistChats()
            self.loadChats()
        } else{
            
            if(completionHandler != nil){
                completionHandler!(ResponseCodes.myChatNotFound)
            }
            
        }
    }
    
    func persistChats(){
        
        let data = self.parseChatsToData(chats: self.myChats)
        
        if data != nil {
            
            self.defaults.set(data, forKey: Constants.CHATS_ARCHIVE)
            self.defaults.synchronize()
        }
        
    }
    func loadChats () {
        if let d = self.defaults.data(forKey: Constants.CHATS_ARCHIVE) {
            self.myChats = self.parseDataToChats(unarchivedData: d) as! [Chat]
        }
        return
    }
    func parseChatsToData(chats : [Chat]) -> Data? {
        return try? NSKeyedArchiver.archivedData(withRootObject: chats, requiringSecureCoding: false)
    }
    
    func parseDataToChats(unarchivedData : Data) -> [Chat] {
        
        return try! NSKeyedUnarchiver.unarchivedObject(ofClasses: [NSArray.self, Chat.self, Message.self], from: unarchivedData) as! [Chat]
        
    }
    
    func parseChats(docs: [QueryDocumentSnapshot]) -> [Chat] {
        var result = [Chat]()
        for doc in docs {
            do {
                let docData = doc.data()
                result.append( try Chat.buildFromDictionary(id: doc.documentID, dict: docData))
            } catch {
                continue
            }
            
        }
        return result
    }
    
    func searchInNearbyChats(name: String) -> [Chat]? {
        let result = nearbyChats.filter({( chat : Chat) -> Bool in
            var stringToSearch = name
            if (name.starts(with: "@")) {
                stringToSearch.remove(at: name.startIndex)
                return chat.id.lowercased().starts(with: stringToSearch.lowercased()) && !chat.hide
            }
            return chat.name.lowercased().starts(with: name.lowercased()) && !chat.hide
        })
        
        return result
    }
    
    func searchInMyChats(name: String) -> [Chat]? {
        let result = myChats.filter({( chat : Chat) -> Bool in
            return chat.name.lowercased().starts(with: name.lowercased())
        })
        
        return result
    }
    
    func getNearbyChatByID(id: String) -> Chat? {
        return nearbyChats.first(where: { (chat: Chat) -> Bool in

            return chat.id == id

        })
    }
    
    func getMyChatIndexByID(id: String) -> Int? {
        return myChats.index(where: { (chat: Chat) -> Bool in
            return chat.id == id
        })
    }
    
    func getMyChatByID(id: String) -> Chat? {
        return myChats.first(where: { (chat: Chat) -> Bool in
            return chat.id == id
        })
    }
    
    func fetchMyChats(controller: UIViewController, completionHandler: @escaping () -> Void) {
        
        self.loadChats()
        completionHandler()
        
        AppManager.instance.apiHandler.getMyChats { (responseCode, chats) in
            if responseCode.getType() != "success" {
                AppManager.instance.alertManager.showErrorAlert(controller: controller, responseCode.getMessage(), title: "Error", onOk: nil)
                print("Error fetching my chats")
            } else {
                self.myChats = chats
                self.persistChats()
            }
            
            completionHandler()
            
        }
    }
    
    func deleteIndexMyChats(index: Int, controller: UIViewController, completionHandler: @escaping () -> Void){
        
        if (!self.myChats.indices.contains(index)){
            completionHandler()
        }
        
        AppManager.instance.apiHandler.deleteChatMyChats(id: self.myChats[index].id) { (responseCode) in
            
            if responseCode.getType() != "success" {

                AppManager.instance.alertManager.showErrorAlert(controller: controller, responseCode.getMessage(), title: "Error", onOk: nil)
            } else {
                self.myChats.remove(at: index)
                self.persistChats()
            }
            
            completionHandler()
        }
    }
    
    
    func canGetIntoChat(location: CLLocationCoordinate2D, chat: Chat) -> Bool {
        let chatPoint = CLLocation(latitude: chat.location.latitude, longitude: chat.location.longitude)
        let currentPoint = CLLocation(latitude: location.latitude, longitude: location.longitude)
        let distanceToChat = currentPoint.distance(from: chatPoint)
        return distanceToChat.isLessThanOrEqualTo(Double(exactly: chat.radius)!)
    }
    
    func startMessageListenerOnChat(chat: Chat, onChangeHandler: @escaping (Message, ChangeTypes) -> Void) -> ListenerRegistration{
        
        let listener = AppManager.instance.apiHandler.addMessagesOnChangeListener(chat: chat) {
            (documentChanges) in
            
            documentChanges.forEach { change in
                
                guard let message = Message(document: change.document) else {
                    return
                }
                
                switch change.type {
                    
                case .added:
                    
                    chat.appendMessage(message)
                    onChangeHandler(message, ChangeTypes.added)
                    
                default:
                    break
                }
            }
        }
        return listener
    }
    
    func getMoreMessagesChat(chat: Chat, start: Int, onChangeHandler: @escaping (Message, ChangeTypes) -> Void) -> ListenerRegistration{
        
        let listener = AppManager.instance.apiHandler.loadMoreMessages(chat: chat, start: start) {
            (documentChanges) in
            
            documentChanges.forEach { change in
                
                guard let message = Message(document: change.document) else {
                    return
                }
                
                switch change.type {
                    
                case .added:
                    chat.prependMessage(message)
                    onChangeHandler(message, ChangeTypes.added)
                    
                default:
                    break
                }
            }
            
        }
        
        return listener
    }
    
    func sendMessageToChat(message: Message, chat: Chat, completitionHandler: ((ResponseCodes) -> Void)?){
        
        AppManager.instance.apiHandler.sendMessage(chat: chat, message: message) { (responseCode) in
            if completitionHandler != nil {
                completitionHandler!(responseCode)
            }
        }
    }
    
    func createChat(chatName:String, image: String?, radius:Int, hide:Bool, location: GeoPoint, expire_date: Timestamp, completionHandler: @escaping (ResponseCodes?, Chat?) -> Void){
        AppManager.instance.apiHandler.createChat(
            chatName: chatName,
            image: image,
            radius: radius,
            hide: hide,
            location: location,
            expire_date: expire_date) { (responseCode, chat) in
                
                if(responseCode!.getType() == "success"){
                    
                    self.myChats.append(chat!)
                    self.persistChats()
                }
                
                completionHandler(responseCode, chat)
        }
    }
}
