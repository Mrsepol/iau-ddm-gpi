//
//  InputManager.swift
//  ChatPoint
//
//  Created by Dany Mota on 21/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit

class InputManager{
    
   
    init(){}
    
    func validateEmptyString(string: String?) -> Bool {
        if string?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return false
        }
        return true
    }
    
    func validateLengthString(string: String?, n: Int!) -> Bool {
        if string?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return false
        }
        if string!.count < n{
            return false
        }
        return true
    }
    
    
    func isValidEmail(string: String) -> Bool {
        return string.isValidEmail()
    }
    
}

extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}
