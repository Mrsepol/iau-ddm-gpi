//
//  UserIconManager.swift
//  ChatPoint
//
//  Created by Joao Marques on 29/12/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import Foundation
class UserIconManager{
    
    let defaults = UserDefaults()
    var userIcons: [UserIcon]

    init(){
        userIcons = []
    }
    
    func persistIcons(){
        
        let data = self.parseIconsToData(icons: self.userIcons)
        
        if data != nil {
            
            self.defaults.set(data, forKey: Constants.USERICONS_ARCHIVE)
            self.defaults.synchronize()
        }
        
    }
    func loadUserIcons () {
        if let d = self.defaults.data(forKey: Constants.USERICONS_ARCHIVE) {
            self.userIcons = self.parseDataToIcons(unarchivedData: d)
        }
        return
    }
    func parseIconsToData(icons : [UserIcon]) -> Data? {
        return try? NSKeyedArchiver.archivedData(withRootObject: icons, requiringSecureCoding: false)
    }
    
    func parseDataToIcons(unarchivedData : Data) -> [UserIcon] {
        
        return try! NSKeyedUnarchiver.unarchivedObject(ofClasses: [NSArray.self, UserIcon.self], from: unarchivedData) as! [UserIcon]
        
    }
    
    func getUserIconByUserID(userID: String) -> UserIcon?{
        return userIcons.first(where: { (userIcon: UserIcon) -> Bool in
            return userIcon.userID == userID
        })
    }
    func getUserIcon(userID: String, completionHandler: @escaping (UserIcon?) -> Void){
        
        if let icon = self.getUserIconByUserID(userID: userID) {
            
            completionHandler(icon)
            return
        }
        
        AppManager.instance.apiHandler.getUserIcon(userID: userID) { (userIcon) in
            
            if let icon = userIcon {
                self.userIcons.append(icon)
                self.persistIcons()
                completionHandler(icon)
                return
            }
            completionHandler(nil)
        }
        
    }
}
