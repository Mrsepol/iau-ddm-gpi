//
//  FirebaseAuthConnector.swift
//  ChatPoint
//
//  Created by formando on 08/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import Foundation
import Firebase
import FBSDKLoginKit

class APIHandler {
    
    //static let apiHandler = APIHandler()
    var db: Firestore?
    var auth: Auth?
    var reference: DocumentReference?
    var currentSnapshot: DocumentSnapshot?
    var prevSnapshot: DocumentSnapshot?
    var firstTime = true
    
    func login(email:String, password:String) -> Void {
        auth!.signIn(withEmail: email, password: password) { (result, error) in
            guard let _ = result?.user else {
                print("Error logging user")
                return
            }
            // Este user tem o .uid -> é do firebase
            print("User logged with success")
            
        }
    }
    
    func initDatabase() {
        self.db = Firestore.firestore()
        self.auth = Auth.auth()
    }
    func signOut(completionHandler: ((ResponseCodes?) -> Void)? = nil) -> Void {
        do{
            try Auth.auth().signOut()
            if FBSDKAccessToken.currentAccessTokenIsActive(){
                let loginManager = FBSDKLoginManager()
                loginManager.logOut()
            }
            AppManager.instance.clearUser()
            if let completionHandler = completionHandler {
                completionHandler(ResponseCodes.signOutSuccess)
            }
            
            
            
        } catch let signOutError as NSError {
            print(signOutError)
            if let completionHandler = completionHandler {
                completionHandler(ResponseCodes.serverError)
            }
        }
    }
    
    func login(email:String, password:String, completionHandler: ((ResponseCodes?) -> Void)?) -> Void {
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            guard let authUser = self.getAuthUser() else {
                if let completionHandler = completionHandler {
                    completionHandler(ResponseCodes.invalidCredentials)
                }
                self.signOut()
                return
            }
            // Obter informacao
            self.getUser(uId: authUser.uid, email: email) { (user) in
                
                guard let user = user else {
                    if let completionHandler = completionHandler {
                        completionHandler(ResponseCodes.serverError)
                    }
                    return
                }
                
                AppManager.instance.setUser(user: user)
                if let completionHandler = completionHandler {
                    
                    completionHandler(ResponseCodes.loginSuccess)
                }
                // Este user tem o .uid -> é do firebase
                print("User logged with success")
            }
            
        }
    }
    
    func resetPassword(email:String, completionHandler: ((ResponseCodes?) -> Void)?) -> Void {
        Auth.auth().sendPasswordReset(withEmail: email, completion: { (error) in
            
            if error != nil {
                if let completionHandler = completionHandler {
                    completionHandler(ResponseCodes.resetPasswordError)
                }
                
            } else {
                if let completionHandler = completionHandler {
                    completionHandler(ResponseCodes.resetPasswordSuccess)
                }
                
            }
            
        })
    }
    func loginWithFacebook(completionHandler: ((ResponseCodes?) -> Void)?) -> Void {
        let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
        
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                print(error)
                if let completionHandler = completionHandler {
                    completionHandler(ResponseCodes.invalidCredentials)
                }
                return
            }
            let authUser = self.getAuthUser()
            
            //Error on login
            if authUser == nil {
                
                if completionHandler != nil{
                    completionHandler!(ResponseCodes.serverError)
                }
                self.signOut()
                return
            }
            let photo = authUser!.photoURL?.absoluteString
            
            
            //Initialize a userdo
            let user = ChatPointUser(uId: authUser!.uid,
                                     email:authUser!.email!,
                                     name:authUser!.displayName!,
                                     profilePhoto:photo,
                                     isPhotoURL: true,
                                     authMethod: AuthMethods.facebook)
            
            //Update firebase
            self.updateUserProfile(user: user) { (responseCode) in
                
                if responseCode!.getType() != "success" {
                    if completionHandler != nil{
                        completionHandler!(ResponseCodes.serverError)
                    }
                    self.signOut()
                    return
                }
                //Update cache
                AppManager.instance.setUser(user: user)
                
                if completionHandler != nil{
                    completionHandler!(ResponseCodes.loginSuccess)
                }
                
                return
                
            }
            
        }
        
    }
    
    func getUserIcon(userID: String, completionHandler: @escaping (UserIcon?) -> Void){
        db!.collection("user_icons").document(userID).getDocument { (document, error) in
            if let document = document,
                document.exists,
                let dict = document.data(),
                let icon = dict["icon"] {
               
                do{
                    
                    completionHandler(UserIcon(userID: userID, icon: try ImageHelper.getImageFromBase64String(string: icon as! String)))
                
                }catch{
                    
                    completionHandler(nil)
                }
               
                return
            }
            
            completionHandler(nil)
            
        }
    }
    func getUser(uId: String, email: String, completionHandler: @escaping (ChatPointUser?) -> Void) {
        
        let docRef = db!.collection("users").document(uId)
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                
                //Make a mutable copy of the NSDictionary
                guard let dict = document.data() else {
                    completionHandler(nil)
                    return
                }
                guard dict["authMethod"] != nil, let authMethod = AuthMethods.getMethodFromString(method: dict["authMethod"] as! String) else {
                    completionHandler(nil)
                    return
                }
                guard let name = dict["name"] else {
                    completionHandler(nil)
                    return
                }
                var isPhotoURL : Bool?
                if dict["isPhotoURL"] != nil {
                    isPhotoURL = dict["isPhotoURL"] as? Bool
                } else {
                    isPhotoURL = nil
                }
                
                let profilePhoto = dict["profilePhoto"]
                
                
                let user = ChatPointUser(uId: uId,
                                         email:email,
                                         name:name as! String,
                                         profilePhoto:profilePhoto as? String,
                                         isPhotoURL: isPhotoURL,
                                         authMethod: authMethod)
                
                completionHandler(user)
                
            } else {
                print("Document does not exist")
                completionHandler(nil)
            }
        }
    }
    
    func getChatByID(id: String, completionHandler: @escaping (Chat?) -> Void) {
        
        let docRef = db!.collection("chats").document(id)
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                
                guard let dict = document.data() else {
                    completionHandler(nil)
                    return
                }
                
                do {
                    completionHandler(try Chat.buildFromDictionary(id: document.documentID, dict: dict) )
                } catch{
                    completionHandler(nil)

                }
                
               
                
            } else {
                print("Document does not exist")
                completionHandler(nil)
            }
        }
    }
    
    func updateUserProfile(user: ChatPointUser, completionHandler: ((ResponseCodes?) -> Void)?)-> Void {
        
        let data = user.toArray()
        let uId = user.getUID()
        
        //UPDATE PROFILE
        db!.collection("users").document(uId).setData(data) { err in
            
            if let err = err {
                if let completionHandler = completionHandler {
                    completionHandler(ResponseCodes.updateUserError)
                }
                print("Error adding document: \(err)")
                return
            }
            
            //If the user has the default photo, dont create an icon
            if user.getProfilePhoto() == nil {
                if let completionHandler = completionHandler {
                    completionHandler(ResponseCodes.updateUserSuccess)
                }
                return
            }
            
            //UPDATE USER ICON
            if(user.isPhotoUrl()){
                user.fetchProfilePhoto { (image) in
                    
                    self.updateUserIcon(userID: user.getUID(), image: image, completionHandler: { (responseCode) in
                        
                        if let completionHandler = completionHandler {
                            
                          
                            completionHandler(responseCode)
                            
                        }
                    })
                }
            } else {
                do {
                    let image = try ImageHelper.getImageFromBase64String(string: user.getProfilePhoto())
                    self.updateUserIcon(userID: user.getUID(), image: image,  completionHandler: { (responseCode) in
                        
                        if let completionHandler = completionHandler {
                            
                            
                            completionHandler(responseCode)
                            
                        }
                    })
                } catch {
                    if let completionHandler = completionHandler {
                        
                        completionHandler(ResponseCodes.updateUserError)
                    }
                }
            }
            
        }
        
    }
    
    func updateUserIcon(userID: String, image: UIImage, completionHandler: ((ResponseCodes?) -> Void)?) {
        
       
        let userIcon = UserIcon(userID: userID, profilePhoto: image)
        let data = userIcon.toArray()
        
        db!.collection("user_icons").document(userID).setData(data) { err in
            
            if let err = err {
                if let completionHandler = completionHandler {
                    completionHandler(ResponseCodes.updateUserError)
                }
                print("Error adding document: \(err)")
                return
            }
            
            if let completionHandler = completionHandler {
                
                completionHandler(ResponseCodes.updateUserSuccess)
            }
            
        }
    }
    
    
    
    private func persistAuthenticatedUser(){
        if(self.isLoggedIn()){
            if let user = self.getAuthUser(){
                
                AppManager.instance.setUser(uId: user.uid, email: user.email!, name: user.displayName!)
            }
        }
    }
    
    func createChat(chatName:String, image: String?, radius:Int, hide:Bool, location: GeoPoint, expire_date: Timestamp, completionHandler: ((ResponseCodes?, Chat?) -> Void)?){
        var ref: DocumentReference? = nil
        
        var users = [String]()
        users.append(AppManager.instance.getUser()!.getUID())
        ref = db!.collection("chats").addDocument(data: [
            "name": chatName,
            "radius": radius,
            "hide": hide,
            "lastMessage": "",
            "img": image ?? nil,
            "location": location,
            "expire_date": expire_date,
            "created_at": NSDate().timeIntervalSince1970,
            "users": users,
            ]) { (err) in
                if let _ = err {
                    if let completionHandler = completionHandler {
                        completionHandler(ResponseCodes.createChatError, nil)
                    }
                    return
                }
                do {
                    let chat = try Chat(id: ref!.documentID, img:  image ?? nil, location: location, name: chatName, radius: radius, hide: hide, created_at: NSDate().timeIntervalSince1970, expire_date: expire_date)
                    
                    if let completionHandler = completionHandler {
                        completionHandler(ResponseCodes.createChatSuccess, chat)
                    }
              
                } catch {
                    if let completionHandler = completionHandler {
                        completionHandler(ResponseCodes.createChatError, nil)
                    }
                    
                }
                
                
                
        }
    }
    
    func isLoggedIn() -> Bool{
        
        return Auth.auth().currentUser != nil
    }
    
    func getMyChats(completionHandler: @escaping (ResponseCodes, [Chat]) -> Void) -> Void {
        
        db!.collection("chats").order(by: "created_at", descending: true).whereField("users", arrayContains: AppManager.instance.getUser()!.getUID()).getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                completionHandler(ResponseCodes.errorFetchChats, [])
            } else {
                var chats = [Chat]()
                for document in querySnapshot!.documents {
                    let dict = document.data()
                    
                    do{
                      chats.append(try Chat.buildFromDictionary(id: document.documentID, dict: dict))
                    }catch {
                        continue
                    }
                  
                }
                completionHandler(ResponseCodes.successFetchChats, chats)
            }
        }
    }
    
    func deleteChatMyChats(id: String, completionHandler: @escaping (ResponseCodes) -> Void) -> Void {
        
        db!.collection("chats").document(id).updateData([
            "users": FieldValue.arrayRemove([AppManager.instance.getUser()!.getUID()])
        ]) { (error) in
            
            if error != nil {
                completionHandler(ResponseCodes.errorDeleteMyChat)
                return
            }
            
            completionHandler(ResponseCodes.successDeleteMyChat)
            return
        }
        
        
    }
    func registerUser(email:String, password:String, name:String, profilePhoto:UIImage?,
                      completionHandler: ((ResponseCodes?) -> Void)?) -> Void {
        auth!.createUser(withEmail: email, password: password) { (result, error) in
            guard let _ = result?.user else {
                print("Error creating new user")
                
                if completionHandler != nil{
                    
                    completionHandler!(ResponseCodes.serverError)
                }
                return
            }
            
            print("User created with success")
            
            
            var photo: String? = nil
            if profilePhoto != nil {
                photo = ImageHelper.getBase64FromImage(image: profilePhoto!)
            }
            
            //Initialize a userdo
            let user = ChatPointUser(uId: Auth.auth().currentUser!.uid,
                                     email:email,
                                     name:name,
                                     profilePhoto:photo,
                                     isPhotoURL: false,
                                     authMethod: AuthMethods.firebase)
            
            //Update firebase
            self.updateUserProfile(user: user) { (responseCode) in
                
                if responseCode!.getType() != "success" {
                    if completionHandler != nil{
                        completionHandler!(ResponseCodes.serverError)
                    }
                    self.signOut()
                    return
                }
                //Update cache
                AppManager.instance.setUser(user: user)
                
                if completionHandler != nil{
                    completionHandler!(ResponseCodes.success)
                }
                
                return
            }
        }
    }
    
    func deleteUser(controller: UIViewController) -> Void {
        
        AppManager.instance.alertManager.showLoadingAlert(controller: controller, "")
        
        let user = Auth.auth().currentUser
        
        let uId = user!.uid
//        Delete profile
        db!.collection("users").document(uId).delete() { err in

            if let err = err {
                AppManager.instance.alertManager.hiddenLoadingAlert(controller: controller) {
                    AppManager.instance.alertManager.showErrorAlert(controller: controller, "Sorry, An error was ocurred! \n Try later!", title: "Error", onOk: nil)
                }
                
                print("Error removing user: \(err)")
            } else { // if success remove user
                print("Profile successfully removed!")
                user?.delete { error in
                    if error != nil {
                        // An error happened.
                        print("Error on Delete")
                        AppManager.instance.alertManager.hiddenLoadingAlert(controller: controller) {
                            AppManager.instance.alertManager.showErrorAlert(controller: controller, "Sorry, An error was ocurred! \n Try later!", title: "Error", onOk: nil)
                        }
                    } else {
                        print("User Deleted")
                        return
                    }
                }
            }
        }
        
    }
    
    func addMessagesOnChangeListener(chat: Chat,
                                     onChangeHandler: @escaping ([DocumentChange]) -> Void) -> ListenerRegistration{
        
        
        
        let listener  = db!.collection("messages")
            .document(chat.id).collection("messages")
            .order(by: "timestamp", descending : true).limit(to: 10).addSnapshotListener { documentSnapshot, error in
                
                guard let document = documentSnapshot else {
                    print("Error fetching document: \(error!)")
                    return
                }
                
                guard let firstSnapshot = documentSnapshot!.documents.last else {
                    // The collection is empty.
                    return
                }
                
                if self.firstTime {
                    self.firstTime = false
                    self.currentSnapshot = documentSnapshot!.documents.last
                }
                
                
                var x = document.documentChanges
                x.reverse()
                
                onChangeHandler(x)
                
        }
        
        return listener
    }
    
    func loadMoreMessages(chat: Chat,start : Int, onChangeHandler: @escaping ([DocumentChange]) -> Void) -> ListenerRegistration{
        
        
        
        let listener  = db!.collection("messages")
            .document(chat.id).collection("messages")
            .order(by: "timestamp", descending : true).limit(to: 10).start(afterDocument: self.currentSnapshot!).addSnapshotListener { documentSnapshot, error in
                
                guard let document = documentSnapshot else {
                    print("Error fetching document: \(error!)")
                    return
                }
                
                guard let lastSnapshot = documentSnapshot!.documents.last else {
                    // The collection is empty.
                    return
                }
                
                var curr = self.currentSnapshot?.documentID
                     var last = lastSnapshot.documentID
               
                   self.currentSnapshot = documentSnapshot!.documents.last
                
                
              
                    onChangeHandler(document.documentChanges)
        }
        
        return listener
    }
    
    
    
    
    
    func sendMessage(chat: Chat, message: Message, completitionHandler: @escaping (ResponseCodes) -> Void) {
        
        let new_doc = db!.collection("messages").document(chat.id).collection("messages").addDocument(data: message.representation) { err in
            
            if let err = err {
                
                print("Error updating document: \(err)")
                
            } else {
                
                print("Document successfully updated")
                
            }
            
        }
        
        
    }
    
    func getAuthUser() -> Firebase.User? {
        return Auth.auth().currentUser
    }
    
    func logout() {
        try! auth!.signOut()
        
    }
    
    
    func addUserToChat(chat: Chat, completionHandler: @escaping (ResponseCodes) -> Void) {
        
        let user_id = AppManager.instance.getUser()!.getUID()
        
        db!.collection("chats").document(chat.id).updateData(["users": FieldValue.arrayUnion([user_id])]) { (error) in
            if error != nil {
                completionHandler(ResponseCodes.errorUpdatingChat)
                return
            }
            
            completionHandler(ResponseCodes.success)
            return
        }
        
        
    }
    
    func setFirstTime(){
        self.firstTime = true
    }
}

