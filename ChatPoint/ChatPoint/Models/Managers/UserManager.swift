//
//  UserController.swift
//  ChatPoint
//
//  Created by João Pereira Marques on 22/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import Foundation

class UserManager {
    
    var name: String?
    var id : String?

    init() {
    }
    
    func setUser(id:String,  name: String){
        self.name = name
        self.id = id
    }
}
