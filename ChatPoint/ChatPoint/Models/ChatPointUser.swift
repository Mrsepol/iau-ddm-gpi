//
//  User.swift
//  ChatPoint
//
//  Created by formando on 14/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import Foundation
import UIKit

class ChatPointUser:  NSObject, NSSecureCoding {
    
    private var uId:String
    private var email:String
    private var name:String
    private var profilePhoto:String?
    private var authMethod:AuthMethods
    private var isPhotoURL:Bool?
    private var radius:Int!
    
    static var supportsSecureCoding: Bool {
        return true
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(profilePhoto, forKey: "profilePhoto")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(uId, forKey: "uId")
        aCoder.encode(authMethod.toString(), forKey: "authMethod")
        aCoder.encode(isPhotoURL, forKey: "isPhotoURL")
        aCoder.encode(radius, forKey: "radius")
    }
    
    required init?(coder aDecoder: NSCoder) {

        if let decodedProfilePic = aDecoder.decodeObject(forKey: "profilePhoto") as? NSString {
            self.profilePhoto =  decodedProfilePic as String
        } else {
            self.profilePhoto = nil
        }
        
        if let radius = aDecoder.decodeObject(forKey: "radius") as? NSInteger {
            self.radius =  radius as Int
        } else {
            self.radius = Constants.DEFAULT_RADIUS
        }

        isPhotoURL = aDecoder.decodeObject(forKey: "isPhotoURL") as? Bool
        
        let uId = (aDecoder.decodeObject(forKey:  "uId") as! String)
        self.uId = uId
        
        
        if let authMethod = (aDecoder.decodeObject(forKey:  "authMethod") as? String){
            self.authMethod =  AuthMethods.getMethodFromString(method: authMethod)!
        } else {
            self.authMethod = AuthMethods.firebase
        }
       
        if let email = (aDecoder.decodeObject(forKey:  "email") as? String){
            self.email = email
        } else {
            self.email = ""
        }
        if let name = (aDecoder.decodeObject(forKey:  "name") as? String){
            self.name = name
        } else {
            self.name = ""
        }
        
       
    }
    func toArray() -> [String:Any]{
        var data = [String:Any]()
        data["name"] = name
        data["authMethod"] = authMethod.toString()
        data["profilePhoto"] = profilePhoto
        data["isPhotoURL"] = isPhotoURL
        return data
    }
    
    convenience init(uId:String, email:String, name:String, profilePhoto: String?, isPhotoURL: Bool?, authMethod: AuthMethods) {
        self.init(uId: uId, email: email, name: name)
        self.profilePhoto = profilePhoto
        self.authMethod = authMethod
        self.isPhotoURL = isPhotoURL
        self.radius = Constants.DEFAULT_RADIUS
    }
   
    init(uId:String, email:String, name:String) {
        self.uId = uId
        self.email = email
        self.name = name
        
        /*REMOVE THIS LINE*/
        self.authMethod = AuthMethods.firebase
        self.radius = Constants.DEFAULT_RADIUS
        /* -- */
    }
    
    func getName() -> String?{
        return name
    }
    
    func getEmail() -> String{
        return email
    }
    func getData(from url: URL, completion: @escaping(Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func fetchProfilePhoto(completionHandler: @escaping ((UIImage) -> Void)){
        
        //Photo is default
        if profilePhoto == nil {
            completionHandler(UIImage(named: "default_profile")!)
            return
        }
        //Photo is URL
        if isPhotoURL != nil && isPhotoURL!{
            guard let url = URL.init(string: profilePhoto!) else {
                completionHandler(UIImage(named: "default_profile")!)
                return
            }
            print(url)
            getData(from: url) { data, response, error in
                guard let data = data, error == nil else {
                    completionHandler(UIImage(named: "default_profile")!)
                    return
                    
                }
                completionHandler(UIImage(data:data)!)
                return
            }
        }
        //Photo is file
        else {
            do{
                completionHandler( try ImageHelper.getImageFromBase64String(string: self.profilePhoto!))
            } catch {
                completionHandler(UIImage(named: "default_profile")!)
            }
        }
    }
    
    func getProfilePhoto() -> String!{
        return self.profilePhoto
    }
    
    func getAuthMethod() -> AuthMethods?{
        return self.authMethod
    }
    
    func getUID() -> String{
        return self.uId
    }
    func getRadius() -> Int{
        return self.radius
    }
    
    func setRadius(radius: Int) -> Void {
        self.radius = radius
    }

    func isPhotoUrl() -> Bool{
        
        if let isUrl = isPhotoURL{
            return isUrl
        }
        return false
    }
    
    
}

