//
//  Chat.swift
//  ChatPoint
//
//  Created by Alexandre Frazao on 28/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore

class Chat: NSObject, NSSecureCoding{
    var id: String
    var img: String? // Base64 image representation
    var icon: UIImage? // Base64 image representation
    var lastMessage: String?
    var location: GeoPoint
    var name: String
    var radius: Int
    var hide: Bool
    var expire_date: Timestamp?
    var created_at: NSDate?
    var users: [String]?
    var messages: [Message]
    
    static var supportsSecureCoding: Bool {
        return true
    }
    
   
    func encode(with aCoder: NSCoder) {
        aCoder.encode(img, forKey: Constants.CHAT_IMG)
        if let icon = icon {
            aCoder.encode(ImageHelper.getBase64FromImage(image: icon), forKey: Constants.CHAT_ICON)
        }
        aCoder.encode(name, forKey: Constants.CHAT_NAME)
        aCoder.encode(id, forKey: Constants.CHAT_ID)
        aCoder.encode(location.latitude, forKey: Constants.CHAT_LAT)
        aCoder.encode(location.longitude, forKey: Constants.CHAT_LNG)
        //aCoder.encode(messages, forKey: Constants.CHAT_MESSAGES)
        aCoder.encode(radius, forKey: Constants.CHAT_RADIUS)
        aCoder.encode(hide, forKey: Constants.CHAT_HIDE)
    }
    
    required init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: Constants.CHAT_ID) as! String
        
        if let img = aDecoder.decodeObject(forKey: Constants.CHAT_IMG) as? NSString {
            self.img =  img as String
        } else {
            self.img = nil
        }
        
        if let icon = aDecoder.decodeObject(forKey: Constants.CHAT_ICON) as? NSString {
            do{
                self.icon =  try ImageHelper.getImageFromBase64String(string: icon as String)
            } catch {
                self.icon = nil
            }
        } else {
            self.icon = nil
        }
        
        let lat = aDecoder.decodeDouble(forKey: Constants.CHAT_LAT) 
        let lng = aDecoder.decodeDouble(forKey: Constants.CHAT_LNG) 
        self.location = GeoPoint(latitude: lat, longitude: lng)
        self.name = aDecoder.decodeObject(forKey: Constants.CHAT_NAME) as! String
        self.radius = aDecoder.decodeInteger(forKey: Constants.CHAT_RADIUS)
        self.hide = aDecoder.decodeBool(forKey: Constants.CHAT_HIDE)
        self.messages = []
    }
    
    func getIcon() -> UIImage{
        if let icon = self.icon {
            return icon
        }
        return UIImage(named: "ChatPointIconXS")!
    }
    
    init(id: String, img: String?, location: GeoPoint, name: String, radius: Int, hide:Bool, created_at: Double, expire_date: Timestamp) throws {
        if(radius <= 0 || radius > 1000){
            throw NSError(domain: "Invalid radius", code: 001, userInfo: nil)
        }
        self.id = id
        self.img = img
        if let img = img {
            self.icon = ImageHelper.resizeImage(image: try ImageHelper.getImageFromBase64String(string: img), targetSize: CGSize(width: 32, height: 32))
        }
        self.location = location
        self.name = name
        self.radius = radius
        self.hide = hide
        self.messages = []
        let myTimeInterval = TimeInterval(created_at)
        self.created_at = NSDate(timeIntervalSince1970: TimeInterval(myTimeInterval))
        self.expire_date = expire_date
    }
    
    convenience init(id: String, img: String?, location: GeoPoint, name: String, radius: Int, hide:Bool, created_at: Double, expire_date: Timestamp, users: [String]?) throws{
        try self.init(id: id, img: img, location: location, name: name, radius: radius, hide: hide, created_at: created_at, expire_date: expire_date)
        self.users = users
     }
    
    
    static func buildFromDictionary(id: String, dict: [String : Any]) throws -> Chat {
        guard
            let location = dict["location"] as? GeoPoint,
            let name =  dict["name"] as? String,
            let radius = dict["radius"] as? Int,
            let hide = dict["hide"] as? Bool,
            let createdAt = dict["created_at"] as? Double,
            let expiresAt = dict["expire_date"] as? Timestamp
        else {
            throw ErrorCodes.invalidChatValues
        }
    
        if let users = dict["users"]  {
            return try Chat(
                id: id,
                img: dict["img"] as? String,
                location: location,
                name: name,
                radius: radius,
                hide: hide,
                created_at: createdAt,
                expire_date: expiresAt,
                users: users as? Array
            )
        }
        
        return try Chat(
            id: id,
            img: dict["img"] as? String,
            location: location,
            name: name,
            radius: radius,
            hide: hide,
            created_at: createdAt,
            expire_date: expiresAt
        )
    }
    func appendMessage(_ message: Message){
        
        self.messages.append(message)
    }
    
    func prependMessage(_ message: Message){
        
        self.messages.insert(message, at: 0)
    }
    
    func deleteMessages(){
        self.messages = []
    }
    
}
