//
//  ErrorCodes.swift
//  ChatPoint
//
//  Created by Alexandre Frazao on 28/01/2019.
//  Copyright © 2019 ChatPoint. All rights reserved.
//

import Foundation
enum ErrorCodes : Error {
    case invalidbase64Image
    case invalidChatRadius
    case invalidChatValues
}
