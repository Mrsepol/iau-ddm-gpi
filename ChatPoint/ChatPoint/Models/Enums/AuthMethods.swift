//
//  AuthMethods.swift
//  ChatPoint
//
//  Created by Joao Marques on 24/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import Foundation
enum AuthMethods {
    case firebase, facebook
    
    func toString() -> String {
        switch self {
        case .firebase:
            return "firebase"
        case .facebook:
            return "facebook"
        }
    }
    
    static func getMethodFromString(method: String) -> AuthMethods? {
        switch method {
        case "firebase":
            return self.firebase
        case "facebook":
             return self.facebook
        default:
            return nil
        }
    }
}
