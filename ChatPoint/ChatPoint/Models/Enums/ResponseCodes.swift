
//  ResponseCodes.swift
//
//
//  Created by João Pereira Marques on 16/11/2018.
//

import Foundation
enum ResponseCodes {
    case success, loginSuccess, invalidCredentials, serverError, signOutSuccess, resetPasswordError, resetPasswordSuccess, successDeleteMyChat,
    registerError, updateUserSuccess, updateUserError, createChatError, createChatSuccess, successFetchChats, errorFetchChats, errorDeleteMyChat,
    myChatNotFound, errorUpdatingChat
    
    func getMessage() -> String {
        switch self {
        case .successFetchChats:
            return "Chats fetched successfully"
        case .errorFetchChats:
            return "An error occurred fetching chats"
        case .success:
            return "Success"
        case .loginSuccess:
            return "Successfully logged in"
        case .invalidCredentials:
            return "Invalid credentials"
        case .serverError:
            return "Server error"
        case .signOutSuccess:
            return "Signed out successfully"
        case .resetPasswordError:
            return "An error occurred on reseting password"
        case .resetPasswordSuccess:
            return "Password reset email was sent"
        case .registerError:
            return "An error occurred at register"
        case .updateUserError:
            return "An error occurred updating the user"
        case .updateUserSuccess:
            return "User updated sucessfully"
        case .createChatError:
            return "An error occurred creating chat"
        case .createChatSuccess:
            return "Chat created successfully"
        case .errorDeleteMyChat:
            return "Error deleting chat"
        case .errorUpdatingChat:
            return "Error updating chat"
        case .successDeleteMyChat:
            return "Successfully deleted chat"
        case .myChatNotFound:
            return "The provided chat does not exist"
        }
        
    }
    
    func getType() -> String {
        
        switch self {
        case .success, .loginSuccess, .signOutSuccess, .resetPasswordSuccess, .updateUserSuccess, .createChatSuccess, .successFetchChats,
             .successDeleteMyChat:
            return "success"
        case .invalidCredentials, .serverError, .resetPasswordError,.registerError, .updateUserError, .createChatError, .errorFetchChats,
             .errorDeleteMyChat, .errorUpdatingChat, .myChatNotFound:
            return "error"
        }
    }
    
}
