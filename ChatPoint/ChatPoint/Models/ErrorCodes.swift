//
//  ErrorCodes.swift
//  ChatPoint
//
//  Created by João Pereira Marques on 16/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import Foundation
enum ErrorCodes {
    case success, invalidCredentials, serverError
}
