//
//  Message.swift
//  ChatPoint
//
//  Created by Hugo freire on 09/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import Foundation
import Firebase
import MessageKit
import FirebaseFirestore

private struct ImageMediaItem: MediaItem {
    
    var url: URL?
    var image: UIImage?
    var placeholderImage: UIImage
    var size: CGSize
    
    init(image: UIImage) {
        self.image = ImageHelper.resizeImage(image: image, targetSize: CGSize(width: 240, height: 240))
        self.size = CGSize(width: 240, height: 240)
        self.placeholderImage = UIImage()
    }
    
}

internal class Message: NSObject, MessageType, NSSecureCoding {
    
    var messageId: String
    var sender: Sender
    var sentDate: Date
    var kind: MessageKind
    
    static var supportsSecureCoding: Bool {
        return true
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.messageId, forKey: Constants.MESSAGE_ID)
        aCoder.encode(self.sentDate.timeIntervalSince1970, forKey: Constants.MESSAGE_SEND_DATE)
        aCoder.encode(self.sender.id, forKey: Constants.MESSAGE_SENDER_ID)
        aCoder.encode(self.sender.displayName, forKey: Constants.MESSAGE_SENDER_DISPLAY_NAME)
        
        var message_kind = ""
        var message_text = ""
        switch self.kind {
        case .text(let text):
            message_kind = "text"
            message_text = text
            break
        case .emoji(let text):
            message_kind = "emoji"
            message_text = text
            break
        case .attributedText(let attributedText):
            message_kind = "attributedText"
            message_text = attributedText.string
            break
        case .photo(let mediaItem):
            message_kind = "photo"
            message_text = ImageHelper.getBase64FromImage(image: mediaItem.image ?? mediaItem.placeholderImage)
            break
        default:
            break
        }
        
       
        aCoder.encode(message_kind, forKey: Constants.MESSAGE_KIND)
        aCoder.encode(message_text, forKey: Constants.MESSAGE_TEXT)

    }
    
    required init?(coder aDecoder: NSCoder) {
        
        self.messageId = aDecoder.decodeObject(forKey: Constants.MESSAGE_ID) as! String
        self.sender = Sender(id: aDecoder.decodeObject(forKey: Constants.MESSAGE_SENDER_ID) as! String,
                             displayName: aDecoder.decodeObject(forKey: Constants.MESSAGE_SENDER_DISPLAY_NAME) as! String)
        self.sentDate = Date(timeIntervalSince1970: aDecoder.decodeDouble(forKey: Constants.MESSAGE_SEND_DATE))
        
        let kind_string = aDecoder.decodeObject(forKey: Constants.MESSAGE_KIND) as! String
        let text = aDecoder.decodeObject(forKey: Constants.MESSAGE_TEXT) as! String
        var kind:MessageKind?
        switch kind_string{
        case "text":
            kind = MessageKind.text(text)
            break
        case "emoji":
            kind = MessageKind.emoji(text)
            break
        case "attributedText":
            kind = MessageKind.attributedText(NSAttributedString(string: text))
            break
        case "photo":
          
            kind = MessageKind.photo(ImageMediaItem(image:
                try! ImageHelper.getImageFromBase64String(string: text)))
            
            break
        default:
            break
            
        }
        
        self.kind = kind!
        
        
    }
    
    private init(kind: MessageKind, sender: Sender, messageId: String, date: Date) {
        self.kind = kind
        self.sender = sender
        self.messageId = messageId
        self.sentDate = date
    }
    
    convenience init(text: String, sender: Sender, messageId: String, date: Date) {
        self.init(kind: .text(text), sender: sender, messageId: messageId, date: date)
    }
    
    convenience init(emoji: String, sender: Sender, messageId: String, date: Date) {
        self.init(kind: .emoji(emoji), sender: sender, messageId: messageId, date: date)
    }
    
    convenience init(image: UIImage, sender: Sender, messageId: String, date: Date) {
        let mediaItem = ImageMediaItem(image: image)
        self.init(kind: .photo(mediaItem), sender: sender, messageId: messageId, date: date)
    }
    
    convenience init?(document: QueryDocumentSnapshot) {
        
        let data = document.data()
        var message = ""
        var senderID = ""
        var senderName = ""
        var date = Date()
        var kind = ""
        
        for messageData in data {
            if(messageData.key == "message"){
                message = messageData.value as! String
            }
            
            if(messageData.key == "user_id"){
                senderID = messageData.value as! String
            }
            
            if(messageData.key == "name"){
                senderName = messageData.value as! String
            }
            
            if(messageData.key == "timestamp"){
                let timestamp : Timestamp = messageData.value as! Timestamp
                date = timestamp.dateValue()
                
            }
            
            if(messageData.key == "kind"){
                kind = messageData.value as! String
            }
            
        }
        
        let message_sender = Sender(id: senderID, displayName: senderName)
        
        let message_online_id = document.documentID
        
        switch kind {
        case "text":
           self.init(kind: .text(message), sender: message_sender, messageId: message_online_id, date: date)
        case "photo":
        
            do {
                self.init(kind: .photo(ImageMediaItem(image: try ImageHelper.getImageFromBase64String(string: message))),
                          sender: message_sender, messageId: message_online_id, date: date)
            } catch {
                self.init(kind: .photo(ImageMediaItem(image:UIImage(named: "gallery")!)),
                          sender: message_sender, messageId: message_online_id, date: date)
            }
           
           
        default:
            self.init(kind: .text(message), sender: message_sender, messageId: message_online_id, date: date)
        }
        
    }
    
}

extension Message: DatabaseRepresentation {
    
    var representation: [String : Any] {
        
        var final_text = "test"
        var final_image:UIImage
        var kind = ""
        
        switch self.kind {
        case .text(let text), .emoji(let text):
            final_text = text
            kind = "text"
        case .attributedText(let attributedText):
            final_text = attributedText.string
            kind = "text"
        case .photo(let mediaItem):
            final_image = mediaItem.image ?? mediaItem.placeholderImage
            final_text = ImageHelper.getBase64FromImage(image: final_image)
            kind = "photo"
        default:
            break
        }
        
        let rep: [String : Any] = [
            "message": final_text,
            "name": self.sender.displayName,
            "user_id": self.sender.id,
            "timestamp": self.sentDate,
            "kind": kind
        ]
        
        return rep
    }
    
}

