//
//  Constants.swift
//  ChatPoint
//
//  Created by João Pereira Marques on 23/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import Foundation

struct Constants {
    
    static let USER_ARCHIVE = "user_archive"
    //CHATS
    static let CHAT_IMG = "img"
    static let CHAT_ICON = "icon"
    static let CHAT_NAME = "name"
    static let CHAT_RADIUS = "radius"
    static let CHAT_HIDE = "hide"
    static let CHAT_EXPIRE_DATE = "expire_date"
    static let CHAT_LAT = "lat"
    static let CHAT_LNG = "lng"
    static let CHAT_ID = "id"
    static let CHATS_ARCHIVE = "chats_archive"
    static let CHAT_MESSAGES = "chats_messages"
    //MESSAGES
    static let MESSAGE_ID = "message_id"
    static let MESSAGE_SEND_DATE = "message_send_date"
    static let MESSAGE_SENDER_DISPLAY_NAME = "message_sender_display_name"
    static let MESSAGE_SENDER_ID = "message_sender_id"
    static let MESSAGE_KIND = "message_kind"
    static let MESSAGE_TEXT = "message_text"
    //USERICON
    static let USERICONS_ARCHIVE = "usericons_archive"
    static let USERICON_USERID = "usericon_userid"
    static let USERICON_ICON = "usericon_image"
    static let USERICON_TIMESTAMP = "usericon_timestamp"
    static let DEFAULT_RADIUS = 500

}
