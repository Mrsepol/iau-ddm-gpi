//
//  CreateChatViewController.swift
//  ChatPoint
//
//  Created by Dany Mota on 14/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase
import FirebaseFirestore
import CoreLocation

class CreateChatViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var chatImage: UIImageView!
    @IBOutlet weak var chatName: UITextField!
    @IBOutlet weak var radiusSegmentedControl: UISegmentedControl!
    
    private var reference: DocumentReference?
    private var blockMultiCreated = false
    
    var imagePicker = UIImagePickerController()
    let locationManager = CLLocationManager()
    var userCoords:CLLocationCoordinate2D?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.requestWhenInUseAuthorization()
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(CreateChatViewController.tapDetected))
        chatImage.isUserInteractionEnabled = true
        chatImage.addGestureRecognizer(singleTap)
    }
    
    //Action
    @objc func tapDetected() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Picking image")
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        locationManager.startUpdatingLocation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationObj = locations.last
        let coordinate =  locationObj?.coordinate
        if let coord = coordinate{
            print("Latitude \(coord.latitude) longitude \(coord.longitude)")
            userCoords = coord
        }
    }
    @IBAction func btnCreateChat(_ sender: Any) {
        let name = chatName.text;
        if !AppManager.instance.inputManager.validateEmptyString(string: name) {
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Please enter a name!", title: "Error")
            return
        }
        var radius = 100
        switch radiusSegmentedControl.selectedSegmentIndex {
        case 1:
            radius = 150
            break
        case 2:
            radius = 200
            break
        case 3:
            radius = 250
            break
        default:
            radius = 100
            break;
        }
        
        if userCoords == nil{
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Sorry, but your location is undefined!\n Ative GPS and try again.", title: "Error")
            return
        }
        if !blockMultiCreated{
            createChat(chatName: name!, radius: radius)
            blockMultiCreated = true;
        }else{
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Sorry, but you must wait!", title: "Error")
            print("Already creatting other chat")
        }
        
    }
    
    // MARK: - fun Create Chat
    
    func createChat(chatName:String, radius:Int){
        
        let db = Firestore.firestore()
        reference = db.collection("chats").addDocument(data: [
            "name": chatName,
            "radius": radius,
            "lastMessage": "",
            "img": ImageHelper.getBase64FromImage(image: chatImage.image!),
            "location": GeoPoint(latitude: userCoords!.latitude, longitude: userCoords!.longitude),
            "ownerID": AppManager.instance.getUser()!.getUID()
        ]) { err in
            self.blockMultiCreated = false
            if let err = err {
                AppManager.instance.alertManager.showErrorAlert(controller: self, "Sorry, error on create chat. \n Please try again later.", title: "Error")
                print("Error adding document: \(err)")
            } else {
                // Add a new document in collection "messages"
                db.collection("messages").document(self.reference!.documentID).setData(["":""]) { err in
                    if let err = err {
                        print("Error writing message: \(err)")
                    } else {
                        print("Message successfully written!")
                    }
                }
                db.collection("chats/" + self.reference!.documentID + "/users").addDocument(data: [
                    "id" : AppManager.instance.getUser()!.getUID(),
                    "name" : AppManager.instance.getUser()!.getName()
                ]) {
                    err in
                    if let err = err {
                        print("Error configuring user subcollection")
                    }
                    AppManager.instance.alertManager.showErrorAlert(controller: self, "Chat created with success", title: "Success")
                    self.chatName.text = ""
                }
                print("Document added with ID: \(self.reference!.documentID)")
            }
        }
    }
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            chatImage.image = ImageHelper.resizeImage(image: image, targetSize: CGSize(width: 200, height: 180))
            //chatImage.image = image
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
}

