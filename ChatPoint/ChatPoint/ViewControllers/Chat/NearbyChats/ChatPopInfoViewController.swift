//
//  ChatPopInfoViewController.swift
//  ChatPoint
//
//  Created by Alexandre Frazao on 21/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit

class ChatPopInfoViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let dialogheigth:CGFloat = self.view.frame.height * 0.5
        let dialogwidth:CGFloat = self.view.frame.width * 0.5
        
        self.preferredContentSize = CGSize(width: dialogwidth, height: dialogheigth)
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
