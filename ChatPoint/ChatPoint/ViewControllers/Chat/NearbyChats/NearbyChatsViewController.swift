//
//  NearbyChatsViewController.swift
//  ChatPoint
//
//  Created by formando on 08/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import FirebaseFirestore

class NearbyChatsViewController: UIViewController, MKMapViewDelegate,  CLLocationManagerDelegate, UIPopoverPresentationControllerDelegate, HalfModalPresentable {
    
    let locationManager = CLLocationManager()
    var currentLocation:CLLocationCoordinate2D?
    let annotation = MKPointAnnotation()
    
    var currentChat:Chat?
    var halfModalTransitioningDelegate: HalfModalTransitionDelegate?
    var enterIntoChat: Bool = false
    var didAskPermission = false
    var radius = Constants.DEFAULT_RADIUS
    @IBOutlet weak var map: MKMapView!
    var firstResponse = true
    var activeAnnotations = [ChatPointAnnotation]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        map.delegate = self
        
        // Do any additional setup after loading the view.
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.requestWhenInUseAuthorization()
        
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        notificationCenter.addObserver(self, selector: #selector(goToChat), name:NSNotification.Name(rawValue: "goToChat"), object: nil)
        self.firstResponse = true
        
        if (!AppManager.instance.hasConnection) {
            AppManager.instance.alertManager.showErrorAlert(controller: self, "No internet connection detected, your information might be outdated", title: "Network error", onOk: nil)
        }
    }
    func addMapTrackingButton(){
        let image = UIImage(named: "user_icon")
        let button   = UIButton(type: UIButton.ButtonType.custom) as UIButton
        button.frame = CGRect(origin: CGPoint(x:5, y: 25), size: CGSize(width: 35, height: 35))
        button.setImage(image, for: .normal)
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(centerMapOnUserButtonClicked), for:.touchUpInside)
        self.map.addSubview(button)
    }
    
    @objc func centerMapOnUserButtonClicked() {
        
        //self.map.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
        if let _ = currentLocation {
            centerOnUser(location: currentLocation!, latMeters: Double(radius) * 2.1 , longMeters: Double(radius) * 2.1)
        }
        
    }
    
    @objc func goToChat() {
        self.performSegue(withIdentifier: "segueToChat", sender: self)
    }
    
    @objc func appMovedToForeground() {
        checkPermissions()
    }
    
    func checkPermissions() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .restricted, .denied:
                AppManager.instance.alertManager.alertToSettings(controller: self, title: "Location access not available", message: "This functionality will need your location to provide nearby chats")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                didAskPermission = true
                locationManager.startUpdatingLocation()
            case .notDetermined:
                return
            }
        } else {
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Location services must be enabled for this functionality to work", title: "Location services not enabled", onOk: nil)
        }
    }
    
    
  
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
       addMapTrackingButton()
    }
    override func viewDidDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //Update user radius
        self.radius = (AppManager.instance.getUser() != nil) ? AppManager.instance.getUser()!.getRadius() : Constants.DEFAULT_RADIUS
    
        locationManager.startUpdatingLocation()
        checkPermissions()
    
        if let curLoc = currentLocation {
            centerOnUser(location: curLoc, latMeters: Double(self.radius) * 2.1, longMeters: Double(self.radius) * 2.1)
            getNearbyChats()
        }
        
        
        if enterIntoChat {
            self.performSegue(withIdentifier: "segueToModal", sender: self)
        }
    
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkPermissions()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationObj = locations.last
        let coordinate =  locationObj?.coordinate
        if let coord = coordinate{
            //print("Latitude \(coord.latitude) longitude \(coord.longitude)")
            currentLocation = coord
            
            if let curLoc = currentLocation {
                // Sabemos a ultima
                if let lastLocation = AppManager.instance.lastKnowLocation {
                    annotation.coordinate = curLoc
                    annotation.title = "Me"
                    map.addAnnotation(annotation)
                    
                    let distance = locationObj!.distance(from: CLLocation(latitude: lastLocation.latitude, longitude: lastLocation.longitude))
                    
                    if (distance > 25) {
                        AppManager.instance.lastKnowLocation = currentLocation
                        getNearbyChats()
                    }
                } else {
                    AppManager.instance.lastKnowLocation = currentLocation
                    centerOnUser(location: curLoc, latMeters: Double(radius*2), longMeters: Double(radius*2))
                    getNearbyChats()
                }
            }
        }
    }
    
    func centerOnUser(location: CLLocationCoordinate2D, latMeters: Double, longMeters: Double) {
        let region = MKCoordinateRegion(center: location, latitudinalMeters: latMeters, longitudinalMeters: longMeters)
        map.setRegion(region, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == "segueToModal" {
            AppManager.instance.selectedChat = currentChat
            (segue.destination.children[0] as! MapModalViewController).chat = currentChat
            (segue.destination.children[0] as! MapModalViewController).currentLocation = currentLocation
            self.halfModalTransitioningDelegate = HalfModalTransitionDelegate(viewController: self, presentingViewController: segue.destination)
            
            segue.destination.modalPresentationStyle = .custom
            segue.destination.transitioningDelegate = self.halfModalTransitioningDelegate
            self.enterIntoChat = false
        }
        
        if segue.identifier == "segueToChat" {
            var chat: Chat
            if currentChat == nil {
                chat = AppManager.instance.selectedChat!
            }else {
                chat = currentChat!
            }
            
            (segue.destination as! ChatViewController).currentChat = chat
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkPermissions()
    }
    
    
    
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        //print("User touched annotation -  showing chat")
        if view.annotation is ChatPointAnnotation {
            //print("User touched chat annotation")
            let ann = view.annotation as! ChatPointAnnotation
            
            if let chat = ann.chat {
                currentChat = chat
                self.performSegue(withIdentifier: "segueToModal", sender: self)
            }
        }
    }
    
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        performSegue(withIdentifier: "segueToChat", sender: self)
    }
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    func updateAreas() -> Void {
        map.removeOverlays(map.overlays)
        for chat in AppManager.instance.chatManager.nearbyChats {
            if chat.hide {
                continue
            }
            let location = chat.location
            createCircleAroundLocation(
                loc: CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude),
                radius: Double(chat.radius),
                color: UIColor(red:0.29, green:0.31, blue:0.31, alpha:0.3))
        }
        if let curr = currentLocation {
            createCircleAroundLocation(loc: curr, radius: Double(radius), color: UIColor.black)
        }
    }
    
    // Funcao teste para adicionar o circulo
    func createCircleAroundLocation(loc:CLLocationCoordinate2D, radius:Double, color: UIColor) -> Void {
        //self.mapView.delegate = self
        let circle = MKCircleColored(center: loc, radius: radius)
        circle.color = color
        map.addOverlay(circle)
    }
    
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if (overlay is MKCircleColored) {
            let circle = overlay as! MKCircleColored
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.strokeColor = circle.color
            circleRenderer.lineWidth = 1
            return circleRenderer
        }
        return MKPolylineRenderer()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "chatPin"
        
        if annotation is MKUserLocation {
            return nil
        }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = false
            
            //annotationView?.image = UIImage(named: "group_chat")
            if annotation is ChatPointAnnotation {
                let chatAnnotation = annotation as! ChatPointAnnotation
                
                if let chat = chatAnnotation.chat {
                    annotationView?.image = UIImage(named: "map_chat_icon")
                }
                
                let enterBTN = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                enterBTN.setImage(UIImage(named: "enterChat"), for: .normal)
                enterBTN.setTitle("Enter", for: .normal)
                annotationView?.rightCalloutAccessoryView = enterBTN
            } else {
                annotationView?.image = UIImage(named: "user_icon")
            }
        } else {
            annotationView?.annotation = annotation
        }
        return annotationView
    }
    
    func getNearbyChats() {
        if currentLocation == nil {
            return
        }
        
        let lat = currentLocation?.latitude
        let lng = currentLocation?.longitude
        
        AppManager.instance.chatManager.updateChats(lat: lat!, lng: lng!, range: Double(radius)/1000.0) { (response) in
            if (response?.getType() == "success") {
                self.addAnnotationsToMap()
            } else {
                print("Erro ao obter chats")
            }
        }
    }
    
    @IBAction func openQRScanner(_ sender: Any) {
        if let qrCodeController = self.storyboard?.instantiateViewController(
            withIdentifier: "QRCodeNavigationController") {
           
            self.present(qrCodeController, animated: false) {
                
            }
           
        }
    }
    func addAnnotationsToMap() -> Void {
        map.removeAnnotations(self.activeAnnotations)
        self.activeAnnotations = [ChatPointAnnotation]()
        for chat in AppManager.instance.chatManager.nearbyChats {
            if chat.hide {
                continue
            }
            let ann = ChatPointAnnotation()
            ann.coordinate = CLLocationCoordinate2D(latitude: chat.location.latitude, longitude: chat.location.longitude)
            ann.title = chat.name
            ann.chatID = chat.id
            ann.chat = chat
            ann.identifier = "chatPin"
            map.addAnnotation(ann)
            self.activeAnnotations.append(ann)
        }
        updateAreas()
    }
}

class ChatPointAnnotation : MKPointAnnotation {
    var identifier: String?
    var chatID: String?
    var chat: Chat?
}

class MKCircleColored : MKCircle {
    var color: UIColor?
}
