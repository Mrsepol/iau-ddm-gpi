//
//  MapModalViewController.swift
//  ChatPoint
//
//  Created by Alexandre Frazao on 23/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit
import CoreLocation
class  MapModalViewController: UIViewController {

    @IBOutlet weak var chatNameLabel: UILabel!
    @IBOutlet weak var radiusLabel: UILabel!
    @IBOutlet weak var peopleOnChatLabel: UILabel!
    var chat: Chat?
    @IBOutlet weak var chatImage: UIImageView!
    
    var currentLocation: CLLocationCoordinate2D?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chatNameLabel.text = chat!.name
        radiusLabel.text = String(chat!.radius)
        peopleOnChatLabel.text = "\(chat?.users?.count ?? 1)"
        // Do any additional setup after loading the view.
        if (chat?.img != nil) {
            do{
                chatImage.image = try ImageHelper.getImageFromBase64String(string: (chat?.img)!)
            } catch {
                chatImage.image = UIImage(named: "ChatPointIcon")
            }
        } else {
            chatImage.image = UIImage(named: "ChatPointIcon")
        }
    }
    
    // TODO: Add chat name, number of people in the chat?, BTN enter chat, chat location?
    @IBAction func didPressCancel(_ sender: Any) {
        if let delegate = navigationController?.transitioningDelegate as? HalfModalTransitionDelegate {
            delegate.interactiveDismiss = false
        }
        dismiss(animated: true, completion: nil)
    }
    @IBAction func goToChat(_ sender: Any) {
        guard let loc = currentLocation else {
            return
        }
        guard let ch = self.chat else {
            return
        }
        if AppManager.instance.chatManager.canGetIntoChat(location: loc, chat: ch) {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let finalView = mainStoryboard.instantiateViewController(withIdentifier: "Nearby") as! NearbyChatsViewController
            finalView.currentChat = chat
            finalView.currentLocation = currentLocation
            finalView.enterIntoChat = true
            
            let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "chatView") as? ChatViewController
            vc2?.currentChat = chat
            
            if let delegate = navigationController?.transitioningDelegate as? HalfModalTransitionDelegate {
                delegate.interactiveDismiss = false
            }
            
            self.dismiss(animated: true) { () in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "goToChat"), object: nil)
            }
        } else {
            // TODO: Call this on go to chat button
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Out of range", title: "Error getting into chat", onOk: nil)
        }
    }
}
