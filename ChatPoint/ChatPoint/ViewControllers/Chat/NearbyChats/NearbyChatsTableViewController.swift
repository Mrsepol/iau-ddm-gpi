//
//  NearbyChatsTableViewController.swift
//  ChatPoint
//
//  Created by Alexandre Frazao on 28/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit
import CoreLocation

class NearbyChatsTableViewController: UITableViewController, UISearchBarDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var searchBar: UISearchBar!

    var userIsSearching = false
    var validChats = [Chat]()
    var results = [Chat]()
    var selectedChat: Chat?
    let locationManager = CLLocationManager()
    var userCoords:CLLocationCoordinate2D?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.requestWhenInUseAuthorization()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        results = AppManager.instance.chatManager.nearbyChats
        parseValidChats()
        locationManager.startUpdatingLocation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationObj = locations.last
        let coordinate =  locationObj?.coordinate
        if let coord = coordinate{
//            print("Latitude \(coord.latitude) longitude \(coord.longitude)")
            userCoords = coord
        }
    }
    
    func parseValidChats() {
        for chat in AppManager.instance.chatManager.nearbyChats {
            if (!chat.hide) {
                validChats.append(chat)
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if (userIsSearching) {
            return results.count
        }
        return validChats.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //print(indexPath.row)
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "chatCell", for: indexPath) as! MyChatListTableViewCell

        // Configure the cell...
        if (userIsSearching) {
            cell.textLabel?.text = results[indexPath.row].name
            cell.chatID = results[indexPath.row].id
        } else {
            cell.textLabel?.text = validChats[indexPath.row].name
            cell.chatID = validChats[indexPath.row].id
        }
        
        return cell
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !AppManager.instance.inputManager.validateEmptyString(string: searchText) {
            results = validChats
        } else {
            if let found = AppManager.instance.chatManager.searchInNearbyChats(name: searchText) {
                results = found
            }
        }
        self.tableView.reloadData()
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        userIsSearching = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        userIsSearching = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        results = AppManager.instance.chatManager.nearbyChats
        userIsSearching = false;
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        userIsSearching = false;
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedChat = results[indexPath.row]
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        let selectedCell = sender as! MyChatListTableViewCell
        
        if let vc = segue.destination as? ChatViewController
        {
            vc.currentChat = AppManager.instance.chatManager.getNearbyChatByID(id: selectedCell.chatID)
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        let selectedCell = sender as! MyChatListTableViewCell
        if AppManager.instance.chatManager.canGetIntoChat(location: userCoords!, chat: AppManager.instance.chatManager.getNearbyChatByID(id: selectedCell.chatID)!) {
            return true
        }
        AppManager.instance.alertManager.showErrorAlert(controller: self, "Out of range", title: "Error getting into chat", onOk: nil)
        return false
    }

}
