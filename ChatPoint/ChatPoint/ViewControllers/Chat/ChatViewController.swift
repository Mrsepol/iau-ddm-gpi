//
//  ChatViewController.swift
//  ChatPoint
//
//  Created by Hugo freire on 06/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit
import MessageKit
import MessageInputBar
import Firebase
import FirebaseFirestore
import AVFoundation

class ChatViewController: MessagesViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var allRecordsLoaded = false
    var isLoadingList = true
    var isInsertingMessage = false
    var usersInChat: [Sender] = []
    var currentChat: Chat?
    var imagePicker = UIImagePickerController()
    var userDefaulName = "anonymus"
    weak var actionToEnable : UIAlertAction?
    
    @IBOutlet weak var socialShareButton: UIBarButtonItem!
    
    @IBAction func share(_ sender: Any) {
        let activityController = UIActivityViewController(activityItems: ["teste"], applicationActivities: nil)
        
        present(activityController, animated: true, completion: nil)
    }
    private var messageListener: ListenerRegistration?
    
    deinit {
        messageListener?.remove()
    }
    
    let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()
    
    let refreshControl = UIRefreshControl()
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         AppManager.instance.apiHandler.setFirstTime()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.currentChat!.name
        
        checkForAutenticatedUser()
        
        imagePicker.delegate = self
        messageInputBar.delegate = self
        
        messageInputBar.setLeftStackViewWidthConstant(to: 35, animated: false)
        messageInputBar.setStackViewItems([makeButton(named: "camera")], forStack: .left, animated: false)
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        scrollsToBottomOnKeyboardBeginsEditing = true // default false
        maintainPositionOnKeyboardFrameChanged = true // default false
        
        messagesCollectionView.addSubview(refreshControl)
        
        messagesCollectionView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(loadMoreMessages), for: .valueChanged)
        
        self.currentChat?.deleteMessages()
        
        self.messageListener = AppManager.instance.chatManager.startMessageListenerOnChat(
            chat: self.currentChat!, onChangeHandler: self.handleChangeMessages)
        
    }
    
    @objc
    func loadMoreMessages() {
        AppManager.instance.chatManager.getMoreMessagesChat(chat: self.currentChat!, start: self.currentChat!.messages.count, onChangeHandler: self.handleChangeMessages2)
        self.refreshControl.endRefreshing()
    }
    
    private func checkForAutenticatedUser() {
        if (Auth.auth().currentUser?.isAnonymous == true || AppManager.instance.getUser() == nil) {
            let alert = UIAlertController(title: "What's your name?", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                self.navigationController?.popToRootViewController(animated: true)
            }))
            
            alert.addTextField(configurationHandler: { textField in
                textField.placeholder = "Input your name here..."
                textField.addTarget(self, action:#selector(self.textChanged(sender:)) , for: .editingChanged)
            })
            
            let ok_action=UIAlertAction(title: "OK", style: .default, handler: { action in
                
                if let name = alert.textFields?.first?.text {
                    
                    Auth.auth().signInAnonymously(completion: { (authDataResult, error) in
                        self.userDefaulName = name
                    })
                }
            })
            
            alert.addAction(ok_action)
            ok_action.isEnabled = false
            self.actionToEnable = ok_action
            self.present(alert, animated: true)
        } else {
            AppManager.instance.apiHandler.addUserToChat(chat: (self.currentChat)!, completionHandler: { (responseCode) in
                return
            })
        }
    }
    
  
    
    @objc func textChanged(sender:UITextField) {
        self.actionToEnable?.isEnabled = (sender.text?.isEmpty == false)
    }
    
    private func handleChangeMessages(message: Message, changeType: ChangeTypes) {
        switch changeType {
            
        case .added:
            
            insertNewMessageInTable(message)
            
        default:
            break
        }
    }
    
    private func handleChangeMessages2(message: Message, changeType: ChangeTypes) {
        switch changeType {
            
        case .added:
            
            insertNewMessageInTable2(message)
            
        default:
            break
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == "segueToDetailsOfChat" {
            (segue.destination.children[0] as! ChatDetailsTableViewController).chat = currentChat!
        }
    }
    
    
    private func makeButton(named: String) -> InputBarButtonItem {
        
        return InputBarButtonItem()
            .configure {
                $0.spacing = .fixed(10)
                $0.image = UIImage(named: named)?.withRenderingMode(.alwaysTemplate)
                $0.setSize(CGSize(width: 25, height: 25), animated: false)
                $0.tintColor = UIColor(white: 0.8, alpha: 1)
            }.onSelected {
                $0.tintColor = .red
            }.onDeselected {
                $0.tintColor = UIColor(white: 0.8, alpha: 1)
            }.onTouchUpInside { _ in
                
                switch AVCaptureDevice.authorizationStatus(for: .video) {
                case .authorized: // The user has previously granted access to the camera.
                    self.takePhoto()
                    return
                case .notDetermined: // The user has not yet been asked for camera access.
                    AVCaptureDevice.requestAccess(for: .video) { granted in
                        if granted {
                            self.takePhoto()
                        }
                    }
                    
                case .denied: // The user has previously denied access.
                    // TODO: Mostrar mensagem de erro?
                    print("User denied access to camera")
                    return
                case .restricted: // The user can't grant access due to restrictions.
                    return
                }
                
        }
    }
    
    func takePhoto() {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        imagePicker.dismiss(animated: true, completion: nil)
        
        picker.dismiss(animated: true, completion: {
            let sel_image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            let message = Message(image: sel_image!, sender: self.currentSender(), messageId: UUID().uuidString, date: Date())
            self.insertNewMessage(message)
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        messagesCollectionView.scrollToBottom()
        
    }
    
    //Envia mensagem
    func insertNewMessage(_ message: Message) {
        if (!AppManager.instance.hasConnection) {
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Can't send messages without internet connection", title: "No network connection", onOk: nil)
            return
        }
        
        AppManager.instance.chatManager.sendMessageToChat(message: message, chat: self.currentChat!) { resultCode in
        
            return
            
        }
    }
    
    //Insere mensagem na view
    func insertNewMessageInTable(_ message: Message) {
        
         self.isInsertingMessage = true
        
        // Reload last section to update header/footer labels and insert a new one
        messagesCollectionView.performBatchUpdates({
            
            messagesCollectionView.insertSections([self.currentChat!.messages.count - 1])
            
            if self.currentChat!.messages.count >= 2 {
                messagesCollectionView.reloadSections([self.currentChat!.messages.count - 2])
            }
            
        }, completion: { [weak self] _ in
            if self?.isLastSectionVisible() == true {
                self?.messagesCollectionView.scrollToBottom(animated: true)
            }
            self?.isInsertingMessage = false
        })
    }
    
    func insertNewMessageInTable2(_ message: Message) {
        
        self.messagesCollectionView.reloadDataAndKeepOffset()
        self.refreshControl.endRefreshing()

    }
    
    func isLastSectionVisible() -> Bool {
        
        guard !self.currentChat!.messages.isEmpty else { return false }
        
        let lastIndexPath = IndexPath(item: 0, section: self.currentChat!.messages.count - 1)
        
        let frame = messagesCollectionView.layoutAttributesForItem(at: lastIndexPath)?.frame ?? .zero
        var rect = messagesCollectionView.convert(frame, to: view)
        
        // substract 100 to make the "visible" area of a cell bigger
        rect.origin.y -= 100
        
        var visibleRect = CGRect(x: messagesCollectionView.bounds.origin.x, y: messagesCollectionView.bounds.origin.y, width:
            messagesCollectionView.bounds.size.width, height:
            messagesCollectionView.bounds.size.height - messagesCollectionView.contentInset.bottom)
        
        visibleRect = messagesCollectionView.convert(visibleRect, to: view)
        return visibleRect.contains(rect)
    }
    
   
    
    
    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        if indexPath.section % 3 == 0 {
            return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
        return nil
    }
    
    func messageTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let name = message.sender.displayName
        return NSAttributedString(string: name, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption1)])
    }
    
    func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let dateString = formatter.string(from: message.sentDate)
        return NSAttributedString(string: dateString, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption2)])
    }
}


extension ChatViewController: MessagesDataSource {
    
    func currentSender() -> Sender {
        if Auth.auth().currentUser?.isAnonymous == true {
            return Sender(id: Auth.auth().currentUser?.uid ?? UUID().uuidString, displayName: self.userDefaulName )
        }
        if let localUser = AppManager.instance.getUser() {
            return Sender(id: localUser.getUID(), displayName:  localUser.getName() ?? "anonymus")
        }
        return Sender(id: Auth.auth().currentUser?.uid ?? UUID().uuidString, displayName:  Auth.auth().currentUser?.displayName ?? "anonymus")
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return self.currentChat!.messages.count
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return self.currentChat!.messages[indexPath.section]
    }
}

// MARK: - MessagesDisplayDelegate
extension ChatViewController: MessagesDisplayDelegate {
    
    // MARK: - Text Messages
    
    func detectorAttributes(for detector: DetectorType, and message: MessageType, at indexPath: IndexPath) -> [NSAttributedString.Key: Any] {
        return MessageLabel.defaultAttributes
    }
    
    func enabledDetectors(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> [DetectorType] {
        return [.url, .address, .phoneNumber, .date, .transitInformation]
    }
    
    
    // MARK: - Location Messages
    func animationBlockForLocation(message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> ((UIImageView) -> Void)? {
        return { view in
            view.layer.transform = CATransform3DMakeScale(2, 2, 2)
            UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: [], animations: {
                view.layer.transform = CATransform3DIdentity
            }, completion: nil)
        }
    }
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        AppManager.instance.userIconManager.getUserIcon(userID: message.sender.id) { (userIcon) in
            
            if let icon = userIcon {
                avatarView.image = icon.icon
                return
            }
            
            avatarView.image = UIImage(named: "default_profile")
        }
        
    }
}

// MARK: - MessagesLayoutDelegate
extension ChatViewController: MessagesLayoutDelegate {
    
    func cellTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 18
    }
    
    func messageTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 20
    }
    
    func messageBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 16
    }
    
}


// MARK: - MessageInputBarDelegate
extension ChatViewController: MessageInputBarDelegate {
    func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
        for component in inputBar.inputTextView.components {
            if let str = component as? String {
                let message = Message(text: str, sender: currentSender(), messageId: UUID().uuidString, date: Date())
                insertNewMessage(message)
            }else if let img = component as? UIImage {
                let message = Message(image: img, sender: currentSender(), messageId: UUID().uuidString, date: Date())
                insertNewMessage(message)
            }
        }
        inputBar.inputTextView.text = String()
        messagesCollectionView.scrollToBottom(animated: true)
    }
}
