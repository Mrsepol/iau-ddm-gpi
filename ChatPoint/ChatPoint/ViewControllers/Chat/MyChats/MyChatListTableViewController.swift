//
//  Created by Dany Mota on 08/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import CoreLocation

class MyChatListTableViewController: UITableViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var newChatBarButton: UIBarButtonItem!
    
    let chatManager =  AppManager.instance.chatManager
    let locationManager = CLLocationManager()
    var userCoords:CLLocationCoordinate2D?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.requestWhenInUseAuthorization()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.newChatBarButton.isEnabled = AppManager.instance.hasConnection

        chatManager.fetchMyChats(controller: self, completionHandler: self.tableView.reloadData)
        locationManager.startUpdatingLocation()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationObj = locations.last
        let coordinate =  locationObj?.coordinate
        if let coord = coordinate{
            //            print("Latitude \(coord.latitude) longitude \(coord.longitude)")
            userCoords = coord
        }
    }
    
    func deleteChat(indexPath: IndexPath){
        chatManager.deleteIndexMyChats(index: indexPath.row, controller: self) {
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            self.tableView.reloadData()
        }
       
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return chatManager.myChats.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatCell", for: indexPath) as! MyChatListTableViewCell
        
        cell.chatID = AppManager.instance.chatManager.myChats[indexPath.row].id
        cell.chatName.text = AppManager.instance.chatManager.myChats[indexPath.row].name
        cell.chatImage.image = AppManager.instance.chatManager.myChats[indexPath.row].getIcon()
        return cell
    }
    
    
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.deleteChat(indexPath: indexPath)
        }
    }
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.destination is ChatViewController {
            let selectedCell = sender as! MyChatListTableViewCell
            
            if let vc = segue.destination as? ChatViewController
            {
                vc.currentChat = chatManager.getMyChatByID(id: selectedCell.chatID)!
            }
        }
    }
    
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if (identifier == ""){
            return true
        }
        let selectedCell = sender as! MyChatListTableViewCell
        let chat = AppManager.instance.chatManager.getNearbyChatByID(id: selectedCell.chatID)
        
        if chat == nil {
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Invalid chat", title: "Error getting into chat", onOk: nil)
            return false
        }
        
        if AppManager.instance.chatManager.canGetIntoChat(location: userCoords!, chat: chat!) {
            return true
        }
        AppManager.instance.alertManager.showErrorAlert(controller: self, "Out of range", title: "Error getting into chat", onOk: nil)
        return false
    }
    
}
