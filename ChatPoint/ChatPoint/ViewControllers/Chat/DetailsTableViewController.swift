//
//  DetailsTableViewController.swift
//  ChatPoint
//
//  Created by Joao Marques on 20/01/2019.
//  Copyright © 2019 ChatPoint. All rights reserved.
//

import UIKit

class DetailsTableViewController: UITableViewController {
    
    var chat: Chat?
    
    @IBOutlet var tableViewDetails: UITableView!
    @IBOutlet weak var labelCreationDate: UILabel!
    @IBOutlet weak var labelNumberUsers: UILabel!
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var labelRadius: UILabel!
    @IBOutlet weak var labelCode: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyy H:mm"
        labelCreationDate.text = dateFormatter.string(from: Date(timeIntervalSince1970: (chat?.created_at?.timeIntervalSince1970)!))
        
        if let n = chat?.users?.count{
            
            labelNumberUsers.text =  "\(n)"
        }else{
            
            labelNumberUsers.text =  "1"
        }
        
        labelLocation.text = "\(String(describing: chat!.location.latitude.stringValue)); \(String(describing: chat!.location.longitude.stringValue))"
        labelRadius.text = "\(String(describing: chat!.radius)) meters"
        labelCode.text = "@" + chat!.id
        
        DispatchQueue.main.async {
            var frame = self.tableViewDetails.frame
            frame.size.height = self.tableViewDetails.contentSize.height
            self.tableViewDetails.frame = frame
        }
    }
}

extension Double {
    
    var stringValue: String {
        
        return String(format: "%.4f", self)
    }
}
