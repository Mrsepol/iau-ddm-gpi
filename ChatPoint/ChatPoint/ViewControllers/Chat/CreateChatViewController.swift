//
//  CreateChatViewController.swift
//  ChatPoint
//
//  Created by Dany Mota on 14/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase
import FirebaseFirestore
import CoreLocation

class CreateChatViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var chatImage: UIImageView!
    @IBOutlet weak var chatName: UITextField!
    @IBOutlet weak var radiusSegmentedControl: UISegmentedControl!
    
    private var blockMultiCreated = false
    private var chat: Chat?
    var imagePicker = UIImagePickerController()
    let locationManager = CLLocationManager()
    var userCoords:CLLocationCoordinate2D?
    var selectedImage : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.requestWhenInUseAuthorization()
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(CreateChatViewController.tapDetected))
        chatImage.isUserInteractionEnabled = true
        chatImage.addGestureRecognizer(singleTap)
        
        self.chatImage.layer.cornerRadius = self.chatImage.frame.height / 4
        self.chatImage.clipsToBounds = true
        self.chatImage.layer.masksToBounds = true
        self.hideKeyboardWhenTappedAround() 
    }
    
    //Action
    @objc func tapDetected() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Picking image")
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        locationManager.startUpdatingLocation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationObj = locations.last
        let coordinate =  locationObj?.coordinate
        if let coord = coordinate{
            print("Latitude \(coord.latitude) longitude \(coord.longitude)")
            userCoords = coord
        }
    }
    @IBAction func btnCreateChat(_ sender: Any) {
        let name = chatName.text;
        if !AppManager.instance.inputManager.validateEmptyString(string: name) {
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Please enter a name!", title: "Error")
            return
        }
        var radius = 100
        switch radiusSegmentedControl.selectedSegmentIndex {
        case 1:
            radius = 150
            break
        case 2:
            radius = 200
            break
        case 3:
            radius = 250
            break
        default:
            radius = 100
            break;
        }
        
        if userCoords == nil{
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Sorry, but your location is undefined!\n Ative GPS and try again.", title: "Error")
            return
        }
        if !blockMultiCreated{
            createChat(chatName: name!, radius: radius)
            blockMultiCreated = true;
        }else{
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Sorry, but you must wait!", title: "Error")
            print("Already creatting other chat")
        }
        
    }
    
    // MARK: - fun Create Chat
    
    func createChat(chatName:String, radius:Int){
        
        var image: String?
        
        if self.selectedImage != nil {
            image = ImageHelper.getBase64FromImage(image: self.selectedImage!)
        } 
        
        AppManager.instance.apiHandler.createChat(
            chatName: chatName,
            image: image,
            radius: radius,
            location: GeoPoint(latitude: userCoords!.latitude, longitude: userCoords!.longitude)) { (responseCode, chat) in
                self.blockMultiCreated = false
                 if(responseCode!.getType() == "error"){
                    self.blockMultiCreated = false
                    AppManager.instance.alertManager.showErrorAlert(controller: self, responseCode!.getMessage(), title: "Error")
                    print("Error adding documen")
                    return
                }
                
                self.chat = chat
                self.performSegue(withIdentifier: "segueToChat", sender: self)
        }
        
    }
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            chatImage.image = ImageHelper.resizeImage(image: image, targetSize: CGSize(width: 200, height: 180))
            self.selectedImage = ImageHelper.resizeImage(image: image, targetSize: CGSize(width: 200, height: 180))
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let chat = self.chat else {
            return
        }
//        // Get the new view controller using segue.destination.
//        // Pass the selected object to the new view controller.
//        let selectedCell = sender as! MyChatListTableViewCell
        
        if let vc = segue.destination as? ChatViewController
        {
            vc.currentChat = chat
        }
    }
    
}

