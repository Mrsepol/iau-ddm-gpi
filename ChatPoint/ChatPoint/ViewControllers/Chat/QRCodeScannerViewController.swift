//
//  QRCodeScannerViewController.swift
//  ChatPoint
//
//  Created by João Pereira Vieira Marques on 21/01/2019.
//  Copyright © 2019 ChatPoint. All rights reserved.
//

import UIKit
import AVFoundation
import CoreLocation

class QRCodeScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, CLLocationManagerDelegate, UIPopoverPresentationControllerDelegate, HalfModalPresentable{
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var video = AVCaptureVideoPreviewLayer()
    var capturedChat: Chat?
    var currentLocation:CLLocationCoordinate2D?
    let locationManager = CLLocationManager()
    var halfModalTransitioningDelegate: HalfModalTransitionDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.requestWhenInUseAuthorization()
        
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
            print("No camera available - Unable to find video device")
            return
        }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed();
            return;
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes =  [AVMetadataObject.ObjectType.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession);
        previewLayer.frame = view.layer.bounds;
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill;
        view.layer.addSublayer(previewLayer);
        
        captureSession.startRunning();
        let notificationCenter = NotificationCenter.default
        
    
        
    }
    
    @objc func goToChat() {
        self.performSegue(withIdentifier: "segueToChat", sender: self)
    }
    func checkPermissions() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .restricted, .denied:
                AppManager.instance.alertManager.alertToSettings(controller: self, title: "Location access not available", message: "This functionality will need your location to provide nearby chats")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                locationManager.startUpdatingLocation()
            case .notDetermined:
                return
            }
        } else {
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Location services must be enabled for this functionality to work", title: "Location services not enabled", onOk: nil)
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkPermissions()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationObj = locations.last
        let coordinate =  locationObj?.coordinate
        if let coord = coordinate{
            //print("Latitude \(coord.latitude) longitude \(coord.longitude)")
            currentLocation = coord
        }
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.locationManager.startUpdatingLocation()
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning();
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection){
        
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            let readableObject = metadataObject as! AVMetadataMachineReadableCodeObject;
            
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: readableObject.stringValue!)
        }
        
        //dismiss(animated: true)
    }
    
    func found(code: String) {
        
        //"goToChatRoom://chat?id=" + chat!.id
        print(code)
        var aux = code.components(separatedBy: "://")
        if(aux.count <= 1){
            print("Invalid url, :// not found")
            self.alertError(error: "Invalid QR Code!") { (alertAction) in
                self.captureSession.startRunning()
            }
            return 
        }
        
        aux = aux[1].components(separatedBy: "?")
        
        if(aux.count <= 1){
            
            print("Invalid url, ? not found")
            self.alertError(error: "Invalid QR Code!") { (alertAction) in
                self.captureSession.startRunning()
                
            }
            return
        }
        
        aux = aux[1].components(separatedBy: "=")
        
        if(aux.count <= 1){
            
            print("Invalid url, = not found")
            self.alertError(error: "Invalid QR Code!") { (alertAction) in
                self.captureSession.startRunning()
                
            }
            return
        }
        
        let chatId = aux[1]
        
        self.foundChat(id: chatId)
        
        
    }
    
    func foundChat(id: String){
        
        
        guard let chat = AppManager.instance.chatManager.getNearbyChatByID(id: id) else {
            self.alertError(error: "Chat unavailable!") { (alertAction) in
                self.captureSession.startRunning()
                
            }
            return
        }
        
        self.capturedChat = chat
  
        //tabcontroller
        let finalView = UIStoryboard(name: "Main", bundle: nil) .instantiateViewController(withIdentifier: "Tab") as! TabBarViewController//
        
        finalView.isFromURL = true
        finalView.chatID = self.capturedChat!.id
        finalView.currentLocation = self.currentLocation
        self.present(finalView, animated: false) {
        }
    }
    
    
    func alertError(error: String, onOk: ((UIAlertAction) -> Void)?) -> Void {
        
        AppManager.instance.alertManager.showErrorAlert(controller: self, error, title: "Erro", onOk: onOk)
        return
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
