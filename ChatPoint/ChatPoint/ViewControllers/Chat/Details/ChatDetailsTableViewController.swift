//
//  ChatDetailsTableViewController.swift
//  ChatPoint
//
//  Created by Dany Mota on 26/01/2019.
//  Copyright © 2019 ChatPoint. All rights reserved.
//

import UIKit

class ChatDetailsTableViewController: UITableViewController {
    
    @IBOutlet weak var qrCode: UIImageView!
    @IBOutlet weak var buttonShare: UIBarButtonItem!
    var chat: Chat?
    
    @IBAction func onShare(_ sender: Any) {
        let text = "goToChatRoom://chat?id=" + chat!.id
        // TESTE: -> gotochatroom://chat?id=4wZ9bjGem9qGRTUx6KX6
        // set up activity view controller
        
        let img = qrCode.image!
     
        let activityViewController = UIActivityViewController(activityItems: [img, text], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func onBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.buildQRCode()
    }
    
    func buildQRCode() {
        let url = "goToChatRoom://chat?id=" + chat!.id
        let data = url.data(using: String.Encoding.ascii)
        
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else {
            return
        }
        qrFilter.setValue(data, forKey: "inputMessage")
        
        guard let qrImage = qrFilter.outputImage else { return }
        
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledQrImage = qrImage.transformed(by: transform)
        
        qrCode.image = UIImage(ciImage: scaledQrImage)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsTableView" {
            let controller = segue.destination as! DetailsTableViewController
            controller.chat = self.chat
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
