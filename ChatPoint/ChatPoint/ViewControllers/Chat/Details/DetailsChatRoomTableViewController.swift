//
//  DetailsChatRoomTableViewController.swift
//  ChatPoint
//
//  Created by Dany Mota on 26/12/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit

class DetailsChatRoomTableViewController: UITableViewController {
    
    @IBOutlet weak var labelChatName: UILabel!
    @IBOutlet weak var imageChat: UIImageView!
    @IBOutlet weak var labelCreatedAt: UILabel!
    @IBOutlet weak var labelNumberusers: UILabel!
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var labelRadius: UILabel!
    @IBOutlet weak var labelIdentifyCode: UILabel!
    @IBOutlet weak var imageQRCode: UIImageView!
    var currentChat: Chat?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelChatName.text = currentChat?.name
        if let image =  currentChat?.img{
            imageChat.image = ImageHelper.getImageFromBase64String(string: image)
        }
        
        self.buildQRCode()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyy H:mm"
        labelCreatedAt.text = dateFormatter.string(from: Date(timeIntervalSince1970: (currentChat?.created_at?.timeIntervalSince1970)!))
        if let n = currentChat?.users?.count{
            labelNumberusers.text =  "\(n)"
        }else{
            labelNumberusers.text =  "1"
        }
        
        labelLocation.text = "\(String(describing: currentChat!.location.latitude.stringValue)); \(String(describing: currentChat!.location.longitude.stringValue))"
        labelRadius.text = "\(String(describing: currentChat!.radius)) meters"
        labelIdentifyCode.text = "@" + currentChat!.id
        
        
        
    }
    func buildQRCode() {
        let url = "teste:/whatever.com"
        let data = url.data(using: String.Encoding.ascii)
        
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else {
            return
        }
        qrFilter.setValue(data, forKey: "inputMessage")
        
        guard let qrImage = qrFilter.outputImage else { return }
        
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledQrImage = qrImage.transformed(by: transform)
        
        imageQRCode.image = UIImage(ciImage: scaledQrImage)

    }
    /* FUNCAO PARA PARTILHAR IMAGEM // PODE SER UTIL PARA O CHAT
     func shareImageButton(_ sender: UIButton) {
     
     // image to share
     let image = UIImage(named: "Image")
     
     // set up activity view controller
     let imageToShare = [ image! ]
     let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
     activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
     
     // exclude some activity types from the list (optional)
     activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
     
     // present the view controller
     self.present(activityViewController, animated: true, completion: nil)
     }
     
     
     */
    
    
    @IBAction func shareChat(_ sender: Any) {
        // text to share
        let text = "goToChatRoom://chat?id=" + currentChat!.id
        // TESTE: -> gotochatroom://chat?id=4wZ9bjGem9qGRTUx6KX6
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        // exclude some activity types from the list (optional)
        //activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func buttonBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}


extension Double {
    
    var stringValue: String {
        
        return String(format: "%.4f", self)
    }
}
