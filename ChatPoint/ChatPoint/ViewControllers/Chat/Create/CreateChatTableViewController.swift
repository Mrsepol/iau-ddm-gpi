//
//  CreateChartTableViewController.swift
//  ChatPoint
//
//  Created by Dany Mota on 20/12/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase
import FirebaseFirestore
import CoreLocation

class CreateChatTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate, UITextFieldDelegate {
    
    
    @IBOutlet weak var chatImage: UIImageView!
    @IBOutlet weak var chatName: UITextField!{
        didSet {
            chatName.setIcon(UIImage(named: "icon_chat")!)
        }
    }
    @IBOutlet weak var chatExpiresAt: UITextField!{
        didSet {
            chatExpiresAt.setIcon(UIImage(named: "calendar")!)
        }
    }
    @IBOutlet weak var radiusSegmentedControl: UISegmentedControl!
    
    private var datePicker: UIDatePicker?
    @IBOutlet weak var stateChatSwitch: UISwitch!
    
    @IBOutlet weak var labelPrivateChat: UILabel!
     private var chat: Chat?
    var imagePicker = UIImagePickerController()
    let locationManager = CLLocationManager()
    var userCoords:CLLocationCoordinate2D?
    var selectedImage : UIImage?
    var expires_date: Timestamp?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker.delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.requestWhenInUseAuthorization()
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(CreateChatTableViewController.tapDetected))
        
        chatImage.isUserInteractionEnabled = true
        chatImage.addGestureRecognizer(singleTap)
        
        self.chatImage.layer.cornerRadius = self.chatImage.frame.height / 4
        self.chatImage.clipsToBounds = true
        self.chatImage.layer.masksToBounds = true
        
        self.hideKeyboardWhenTappedAround()
        chatName.delegate = self
        
        self.expires_date = nil
        stateChatSwitch.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
        labelPrivateChat.isHidden = true
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .dateAndTime
        datePicker?.minimumDate = Date().adding(minutes: 30)
        datePicker?.addTarget(self, action: #selector(CreateChatTableViewController.dateChanged(datePicker:)), for: .valueChanged)
        chatExpiresAt.inputView = datePicker
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CreateChatTableViewController.viewTapped(gestureRecogniser:)))
        view.addGestureRecognizer(tapGesture)
        
    }
    
    @objc func viewTapped(gestureRecogniser: UITapGestureRecognizer){
        view.endEditing(true)
    }

    @objc func dateChanged(datePicker: UIDatePicker){
        let dateFormatter =  DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy H:m"
        chatExpiresAt.text = dateFormatter.string(from: datePicker.date)
        expires_date = Timestamp.init(date: datePicker.date)
        self.datePicker?.minimumDate = Date().adding(minutes: 30)
//        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func switchChanged(mySwitch: UISwitch) {
        if(mySwitch.isOn) {
            labelPrivateChat.isHidden = false
        }else {
            labelPrivateChat.isHidden = true
        }
    }
    
    //Action
    @objc func tapDetected() {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let defaultAction = UIAlertAction(title: "Take Photo", style: .default, handler: { action in
            switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized: // The user has previously granted access to the camera.
                self.takePhoto()
                return
            case .notDetermined: // The user has not yet been asked for camera access.
                AVCaptureDevice.requestAccess(for: .video) { granted in
                    if granted {
                        self.takePhoto()
                    }
                }
                
            case .denied: // The user has previously denied access.
                // TODO: Mostrar mensagem de erro?
                print("User denied access to camera")
                return
            case .restricted: // The user can't grant access due to restrictions.
                return
            }
        })
        let galleryAction = UIAlertAction(title: "Choose Photo", style: .default, handler: { action in
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Picking image")
                self.imagePicker.sourceType = .savedPhotosAlbum;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        alertController.addAction(galleryAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        locationManager.startUpdatingLocation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationObj = locations.last
        let coordinate =  locationObj?.coordinate
        if let coord = coordinate{
//            print("Latitude \(coord.latitude) longitude \(coord.longitude)")
            userCoords = coord
        }
    }
    @IBAction func galleryButton(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Picking image")
            
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func cameraButton(_ sender: Any) {
        
        // Pedir autorizacao antes de tirar foto
    }
    
    func takePhoto() {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        DispatchQueue.main.async {
            self.present(vc, animated: true)
        }
    }
    
    @IBAction func btnCreateChat(_ sender: Any) {
        let name = chatName.text;
        if !AppManager.instance.inputManager.validateEmptyString(string: name) {
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Please enter a name!", title: "Error", onOk: nil)
            return
        }
        if expires_date == nil{
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Please enter a expire date!", title: "Error", onOk: nil)
            return
        }
        if expires_date!.dateValue() <= Date().adding(minutes: 25){
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Please enter a valid expire date!", title: "Error", onOk: nil)
            return
        }
        var radius = 100
        switch radiusSegmentedControl.selectedSegmentIndex {
        case 1:
            radius = 250
            break
        case 2:
            radius = 500
            break
        case 3:
            radius = 1000
            break
        default:
            radius = 100
            break;
        }
        
        if userCoords == nil{
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Sorry, but your location is undefined!\n Ative GPS and try again.", title: "Error", onOk: nil)
            return
        }
        AppManager.instance.alertManager.showLoadingAlert(controller: self, "Creating your Chat")
        createChat(chatName: name!, radius: radius)

    }
    
    // MARK: - fun Create Chat
    
    func createChat(chatName:String, radius:Int){
        
        var image: String?
        
        if self.selectedImage != nil {
            image = ImageHelper.getBase64FromImage(image: self.selectedImage!)
        }
        
        AppManager.instance.chatManager.createChat(
            chatName: chatName,
            image: image,
            radius: radius,
            hide: self.stateChatSwitch.isOn,
            location: GeoPoint(latitude: userCoords!.latitude, longitude: userCoords!.longitude),
            expire_date: expires_date!) { (responseCode, chat) in
                
                if(responseCode!.getType() == "error"){
                    AppManager.instance.alertManager.hiddenLoadingAlert(controller: self, completion: {
                        AppManager.instance.alertManager.showErrorAlert(controller: self, responseCode!.getMessage(), title: "Error", onOk: nil)
                    })
                    print("Error adding documen")
                    return
                }
                self.cleanData()
                AppManager.instance.alertManager.hiddenLoadingAlert(controller: self) { () in
                    self.chat = chat
                    let appManager = AppManager.instance
                    appManager.chatManager.updateChats(lat: appManager.lastKnowLocation!.latitude, lng: appManager.lastKnowLocation!.longitude, range: Double(appManager.user!.getRadius()), completionHandler: nil)
                self.performSegue(withIdentifier: "segueToChat", sender: self)
            }
        }
    }
    
    func cleanData(){
        chatName.text = ""
        chatExpiresAt.text = ""
        self.expires_date = nil
        radiusSegmentedControl.selectedSegmentIndex = 0
        self.selectedImage = nil
        self.chatImage.image = UIImage(named: "noImage")
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            chatImage.image = ImageHelper.resizeImage(image: image, targetSize: CGSize(width: 200, height: 150))
            self.selectedImage = ImageHelper.resizeImage(image: image, targetSize: CGSize(width: 200, height: 150))
        }
        picker.dismiss(animated: true, completion: nil)
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let chat = self.chat else {
            return
        }
        
        if let vc = segue.destination as? ChatViewController
        {
            vc.currentChat = chat
        }
    }
}

extension Date {
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
}
