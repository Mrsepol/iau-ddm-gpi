//
//  RegisterUserViewController.swift
//  ChatPoint
//
//  Created by formando on 08/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase
import FirebaseFirestore


class RegisterUserViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var passwordConfirmation: UITextField!
    @IBOutlet weak var displayName: UITextField!
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        self.hideKeyboardWhenTappedAround() 
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cancelClick(_ sender: Any) {
        //        self.navigationController?.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneClick(_ sender: Any) {
        if emailField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
                || passwordField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
                || passwordConfirmation.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
                || displayName.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            
            AppManager.instance.alertManager.showErrorAlert(controller: self, "All fields must be filled", title: "Error")
            return
        }
        
        if (passwordField.text != passwordConfirmation.text) {
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Passwords don't match", title: "Error")
            return
        }
        AppManager.instance.alertManager.showLoadingAlert(controller: self, "Creating new account")
        AppManager.instance.apiHandler.registerUser(email: emailField.text!, password: passwordField.text!, name: displayName.text!, profilePhoto: profilePhoto.image!) {
            (responseCode) in
            AppManager.instance.alertManager.hiddenLoadingAlert(controller: self, completion: nil)
            if responseCode!.getType() == "success" {
                //print(responseCode!.getMessage())
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
                return
            }
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Couldn't create account", title: "Error")
            self.view.endEditing(true)
            print(responseCode!.getMessage())
            return
            
        }
        view.endEditing(true)

    }
    
    @IBAction func chooseFromLibrary() {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Picking image")
            
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false

            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            profilePhoto.layer.cornerRadius = profilePhoto.frame.height / 2
            profilePhoto.clipsToBounds = true
            profilePhoto.layer.masksToBounds = true
            profilePhoto.image = ImageHelper.resizeImage(image: image, targetSize: CGSize(width: 200, height: 200))
        }
    
        picker.dismiss(animated: true, completion: nil)
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func takePictureClick(_ sender: Any) {
        
        // Pedir autorizacao antes de tirar foto
        
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized: // The user has previously granted access to the camera.
            takePhoto()
            return
        case .notDetermined: // The user has not yet been asked for camera access.
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    self.takePhoto()
                }
            }
            
        case .denied: // The user has previously denied access.
            // TODO: Mostrar mensagem de erro?
            print("User denied access to camera")
            return
        case .restricted: // The user can't grant access due to restrictions.
            return
        }
    }
    
    func takePhoto() {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
    }
}
