//
//  RegisterUserTableViewController.swift
//  ChatPoint
//
//  Created by Dany Mota on 14/12/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase
import FirebaseFirestore

class RegisterUserTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var emailField: UITextField!{
        didSet {
            emailField.setIcon(UIImage(named: "icon_email")!)
        }
    }
    @IBOutlet weak var passwordField: UITextField!{
        didSet {
            passwordField.setIcon(UIImage(named: "icon_password")!)
        }
    }
    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var passwordConfirmation: UITextField!{
        didSet {
            passwordConfirmation.setIcon(UIImage(named: "icon_password")!)
        }
    }
    @IBOutlet weak var displayName: UITextField!{
        didSet {
            displayName.setIcon(UIImage(named: "default_profile")!)
        }
    }
    
    var hasPhoto = false
    
    var imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        iniComponents()
    }
    
    func iniComponents() -> Void {
        imagePicker.delegate = self
        emailField.delegate = self
        passwordField.delegate = self
        passwordConfirmation.delegate = self
        displayName.delegate = self
        
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(RegisterUserTableViewController.tapDetected))
        profilePhoto.isUserInteractionEnabled = true
        profilePhoto.addGestureRecognizer(singleTap)
        
        
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButton(_ sender: Any) {
        
        if emailField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
            || passwordField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
            || passwordConfirmation.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
            || displayName.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            
            AppManager.instance.alertManager.showErrorAlert(controller: self, "All fields must be filled", title: "Error", onOk: nil)
            return
        }
        
        if !AppManager.instance.inputManager.validateLengthString(string: passwordField.text, n: 8){
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Please enter a valid password. \n At least 8 characters!", title: "Error", onOk: nil)
            return
        }
        
        if (passwordField.text != passwordConfirmation.text) {
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Passwords don't match", title: "Error", onOk: nil)
            return
        }
        guard let name = displayName.text else {
            return
        }
        
        DispatchQueue.main.async {
            AppManager.instance.alertManager.showLoadingAlert(controller: self, "Creating account")
            
            var img : UIImage?
            if (self.hasPhoto)  {
                img = self.profilePhoto.image!
            } else {
                img = nil
            }
            AppManager.instance.apiHandler.registerUser(email: self.emailField.text!, password: self.passwordField.text!, name: name, profilePhoto: img) {
                (responseCode) in
                
                AppManager.instance.alertManager.hiddenLoadingAlert(controller: self) { () in
                    
                    if responseCode!.getType() == "success" {
                        //print(responseCode!.getMessage())
                        let finalView = UIStoryboard(name: "Main", bundle: nil) .instantiateViewController(withIdentifier: "Tab") as! TabBarViewController//NearbyChatsViewController//
                        
                        self.present(finalView, animated: false, completion: nil)
                    }else{
                        AppManager.instance.alertManager.showErrorAlert(controller: self, "Ops, looks like you already have an account!", title: "Error", onOk: nil)
                    }
                }
            }
        }
        view.endEditing(true)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            profilePhoto.layer.cornerRadius = profilePhoto.frame.height / 2
            profilePhoto.clipsToBounds = true
            profilePhoto.layer.masksToBounds = true
            profilePhoto.image = ImageHelper.resizeImage(image: image, targetSize: CGSize(width: 200, height: 200))
            self.hasPhoto = true
        }
        
        picker.dismiss(animated: true, completion: nil)
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    func takePhoto() {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        DispatchQueue.main.async {
            self.present(vc, animated: true)
        }
    }
    
    @objc func tapDetected() {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let defaultAction = UIAlertAction(title: "Take Photo", style: .default, handler: { action in
            
            switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized: // The user has previously granted access to the camera.
                self.takePhoto()
                return
            case .notDetermined: // The user has not yet been asked for camera access.
                AVCaptureDevice.requestAccess(for: .video) { granted in
                    if granted {
                        self.takePhoto()
                    }
                }
                
            case .denied: // The user has previously denied access.
                // TODO: Mostrar mensagem de erro?
                print("User denied access to camera")
                return
            case .restricted: // The user can't grant access due to restrictions.
                return
            }
        })
        let galleryAction = UIAlertAction(title: "Choose Photo", style: .default, handler: { action in
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Picking image")
                self.imagePicker.sourceType = .savedPhotosAlbum;
                self.imagePicker.allowsEditing = false
                DispatchQueue.main.async {
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        alertController.addAction(galleryAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == displayName {
            emailField.becomeFirstResponder()
            return false
        }
        if textField == emailField {
            passwordField.becomeFirstResponder()
            return false
        }
        if textField == passwordField {
            passwordConfirmation.becomeFirstResponder()
            return false
        }
        if textField == displayName {
            emailField.becomeFirstResponder()
            return false
        }
        
        textField.resignFirstResponder()
        return true
    }
    
}

