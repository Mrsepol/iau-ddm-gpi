//
//  EditAccountTableViewController.swift
//  ChatPoint
//
//  Created by Dany Mota on 25/01/2019.
//  Copyright © 2019 ChatPoint. All rights reserved.
//

import UIKit
import FirebaseAuth
import AVFoundation

class EditAccountTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    @IBOutlet weak var userPhotoImage: UIImageView!
    @IBOutlet weak var userNameInput: UITextField!{
        didSet {
            userNameInput.setIcon(UIImage(named: "default_profile")!)
        }
    }
    @IBOutlet weak var userEmailInput: UITextField!{
        didSet {
            userEmailInput.setIcon(UIImage(named: "icon_email")!)
        }
    }
    @IBOutlet weak var backNavicationBtn: UIBarButtonItem!
    @IBOutlet weak var deleteAccountBtn: UIButton!
    
    private var hasChangedImage = false
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(EditAccountTableViewController.tapDetected))
        userPhotoImage.isUserInteractionEnabled = true
        userPhotoImage.addGestureRecognizer(singleTap)
        
        deleteAccountBtn.center = CGPoint.init(x: super.view.center.x, y: (((self.view.center.y/8 * 7) * 2)) < 375 ? 375 : (self.view.center.y/8 * 7) * 2 - 25)
        self.view.addSubview(deleteAccountBtn)
        
        self.userPhotoImage.layer.cornerRadius = self.userPhotoImage.frame.height / 2
        self.userPhotoImage.clipsToBounds = true
        self.userPhotoImage.layer.masksToBounds = true
        self.hideKeyboardWhenTappedAround()
        
        self.reloadData()
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        if(UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight){
            deleteAccountBtn.center = CGPoint.init(x: super.view.center.x, y: 375)
        }else if(UIDevice.current.orientation == .portrait){
            deleteAccountBtn.center = CGPoint.init(x: super.view.center.x, y: (((self.view.center.y/8 * 7) * 2)) < 375 ? 375 : (self.view.center.y/8 * 7) * 2 - 25)
        }
    }
    
    
    /*
     ██╗      ██████╗  █████╗ ██████╗     ██╗   ██╗███████╗███████╗██████╗     ██████╗  █████╗ ████████╗ █████╗
     ██║     ██╔═══██╗██╔══██╗██╔══██╗    ██║   ██║██╔════╝██╔════╝██╔══██╗    ██╔══██╗██╔══██╗╚══██╔══╝██╔══██╗
     ██║     ██║   ██║███████║██║  ██║    ██║   ██║███████╗█████╗  ██████╔╝    ██║  ██║███████║   ██║   ███████║
     ██║     ██║   ██║██╔══██║██║  ██║    ██║   ██║╚════██║██╔══╝  ██╔══██╗    ██║  ██║██╔══██║   ██║   ██╔══██║
     ███████╗╚██████╔╝██║  ██║██████╔╝    ╚██████╔╝███████║███████╗██║  ██║    ██████╔╝██║  ██║   ██║   ██║  ██║
     ╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝      ╚═════╝ ╚══════╝╚══════╝╚═╝  ╚═╝    ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝
     
     */
    
    func getData(from url: URL, completion: @escaping(Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: URL){
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else {return}
            DispatchQueue.main.async {
                self.userPhotoImage.image = ImageHelper.resizeImage(image: UIImage(data:data)!, targetSize: CGSize(width: 150, height: 150))
                
            }
        }
    }
    
    
    fileprivate func reloadData() {
        //nameLabel.text = Auth.auth().currentUser?.displayName
        
        if let localUser = AppManager.instance.getUser() {
            self.userNameInput.text = localUser.getName()
            //userUserNameLabel.text = user.getUserName()
            self.userEmailInput.text = localUser.getEmail()
            
            localUser.fetchProfilePhoto { (image) in
                self.userPhotoImage.image = ImageHelper.resizeImage(image: image, targetSize: CGSize(width: 150, height: 150))
                
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.userPhotoImage.layer.cornerRadius = self.userPhotoImage.frame.height / 2
            self.userPhotoImage.clipsToBounds = true
            self.userPhotoImage.layer.masksToBounds = true
            self.userPhotoImage.image = ImageHelper.resizeImage(image: image, targetSize: CGSize(width: 150, height: 150))
            self.hasChangedImage = true
        }
        picker.dismiss(animated: true, completion: nil)
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func takePhoto() {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        DispatchQueue.main.async {
            self.present(vc, animated: true)
        }
    }
    
    //Click on image
    @objc func tapDetected() {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let defaultAction = UIAlertAction(title: "Take Photo", style: .default, handler: { action in
            switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized: // The user has previously granted access to the camera.
                self.takePhoto()
                return
            case .notDetermined: // The user has not yet been asked for camera access.
                AVCaptureDevice.requestAccess(for: .video) { granted in
                    if granted {
                        self.takePhoto()
                    }
                }
                
            case .denied: // The user has previously denied access.
                // TODO: Mostrar mensagem de erro?
                print("User denied access to camera")
                return
            case .restricted: // The user can't grant access due to restrictions.
                return
            }
        })
        let galleryAction = UIAlertAction(title: "Choose Photo", style: .default, handler: { action in
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Picking image")
                self.imagePicker.sourceType = .savedPhotosAlbum;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        alertController.addAction(galleryAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    /*
     ████████╗ ██████╗ ██████╗      ██████╗ ██████╗ ████████╗██╗ ██████╗ ███╗   ██╗███████╗
     ╚══██╔══╝██╔═══██╗██╔══██╗    ██╔═══██╗██╔══██╗╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝
        ██║   ██║   ██║██████╔╝    ██║   ██║██████╔╝   ██║   ██║██║   ██║██╔██╗ ██║███████╗
        ██║   ██║   ██║██╔═══╝     ██║   ██║██╔═══╝    ██║   ██║██║   ██║██║╚██╗██║╚════██║
        ██║   ╚██████╔╝██║         ╚██████╔╝██║        ██║   ██║╚██████╔╝██║ ╚████║███████║
        ╚═╝    ╚═════╝ ╚═╝          ╚═════╝ ╚═╝        ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝
     
     */
    
    @IBAction func backNavigationButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deleteAccountButton(_ sender: Any) {
        //        delete my account
        AppManager.instance.apiHandler.deleteUser(controller: self)
        AppManager.instance.makeUserLogout(controller: self, showAlertOnSucess: false)
        AppManager.instance.alertManager.hiddenLoadingAlert(controller: self, completion: nil)
        
    }
    @IBAction func saveBarButton(_ sender: Any) {
        let name = userNameInput.text
        if !AppManager.instance.inputManager.validateEmptyString(string: name) {
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Please enter a name!", title: "Error", onOk: nil)
            return
        }
        let email = userEmailInput.text
        if !AppManager.instance.inputManager.isValidEmail(string: email!) {
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Please enter a valid email!!", title: "Error", onOk: nil)
            return
        }
        updateUserOnline(name: name!, email: email!, photo: ImageHelper.getBase64FromImage(image: self.userPhotoImage.image!))
    }
    
    
    
    
    /*
     ███████╗ █████╗ ██╗   ██╗███████╗
     ██╔════╝██╔══██╗██║   ██║██╔════╝
     ███████╗███████║██║   ██║█████╗
     ╚════██║██╔══██║╚██╗ ██╔╝██╔══╝
     ███████║██║  ██║ ╚████╔╝ ███████╗
     ╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚══════╝
     */
    
    
    func updateUserOnline(name: String, email: String, photo: String){
        AppManager.instance.alertManager.showLoadingAlert(controller: self, "" )
        //Initialize a userdo
        if let user = AppManager.instance.getUser() {
            let userToUpdate = ChatPointUser(uId: user.getUID(),
                                             email: email,
                                             name: name,
                                             profilePhoto: self.hasChangedImage == true ? photo : user.getProfilePhoto(),
                                             isPhotoURL: self.hasChangedImage == true ? false : user.isPhotoUrl() ,
                                             authMethod: user.getAuthMethod()!)
            
            AppManager.instance.apiHandler.updateUserProfile(user: userToUpdate) { (responseCode) in
                if responseCode!.getType() == "success" {
                    AppManager.instance.setUser(user: userToUpdate)
                    self.dismiss(animated: true, completion: nil) // Hidden loader
                    self.dismiss(animated: true, completion: nil) // Hidden Activity
                }else{
                    AppManager.instance.alertManager.showErrorAlert(controller: self, ResponseCodes.updateUserError.getMessage(), title: "Error", onOk: nil)
                    return
                }
            }
        }
    }
}
