//
//  MyAccountTableViewController.swift
//  ChatPoint
//
//  Created by Dany Mota on 25/01/2019.
//  Copyright © 2019 ChatPoint. All rights reserved.
//

import UIKit
import FirebaseAuth
class MyAccountTableViewController: UITableViewController {

    @IBOutlet weak var editAccountBarButton: UIBarButtonItem!
    
    @IBOutlet weak var labelRadius: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var logoutButton: UIButton!
    var radius: Int?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImage.layer.cornerRadius = profileImage.frame.height / 2
        profileImage.clipsToBounds = true
        profileImage.layer.masksToBounds = true
        
        logoutButton.center = CGPoint.init(x: super.view.center.x, y: ((self.view.center.y/8 * 7) * 2 - 85) < 450 ? 450 : (self.view.center.y/8 * 7) * 2 - 85)
        self.view.addSubview(logoutButton)
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        
        if(UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight){
            logoutButton.center = CGPoint.init(x: super.view.center.x, y: 450)
        }else if(UIDevice.current.orientation == .portrait){
            logoutButton.center = CGPoint.init(x: super.view.center.x, y: ((self.view.center.y/8 * 7) * 2 - 85) < 450 ? 450 : (self.view.center.y/8 * 7) * 2 - 85)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        guard let r = self.radius else {
            return
        }
        let user = AppManager.instance.user
        if r != user!.getRadius(){
            AppManager.instance.persistUser()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.reloadData()
        self.editAccountBarButton.isEnabled = AppManager.instance.hasConnection
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.reloadData()
    }
    
    @IBAction func editBarButton(_ sender: Any) {
        performSegue(withIdentifier: "segueToEditProfile", sender: self)
    }
    
    @IBAction func logoutClick(_ sender: Any) {
        self.radius = nil
        AppManager.instance.makeUserLogout(controller: self, showAlertOnSucess: true)
    }
    
    fileprivate func reloadData() {
        print("ReloadData")
        if let user = AppManager.instance.getUser() {
            labelName.text = user.getName()
            //userUserNameLabel.text = user.getUserName()
            labelEmail.text = user.getEmail()
            self.radius = user.getRadius()
            
            self.slider.setValue(Float(radius!), animated: false)
            labelRadius.text = String( radius! ) + " m"
            
            user.fetchProfilePhoto { (image) in
                if image != nil {
                    DispatchQueue.main.async {
                        self.profileImage.image = ImageHelper.resizeImage(image: image, targetSize: CGSize(width: 200, height: 150))
                    }
                }
                
            }
            
        }
    }
    @IBAction func sliderChanged(_ sender: UISlider) {
        let fixed = roundf(sender.value / 50.0) * 50.0;
        sender.setValue(fixed, animated: true)
        self.radius = Int(sender.value)
        labelRadius.text = String( self.radius! ) + " m"
        if let radius = self.radius {
            AppManager.instance.user?.setRadius(radius: radius)
        }
    }
}
