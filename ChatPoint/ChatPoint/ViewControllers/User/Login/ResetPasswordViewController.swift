//
//  ResetPasswordViewController.swift
//  ChatPoint
//
//  Created by João Pereira Marques on 08/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
class ResetPasswordViewController: UIViewController {
    
    @IBOutlet weak var emailInput: UITextField!{
        didSet {
            emailInput.setIcon(UIImage(named: "icon_email")!)
        }
    }
    @IBOutlet weak var backButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround() 
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickSubmit(_ sender: Any) {
        let email = self.emailInput.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if email == "" {
            let alertController = UIAlertController(title: "Error", message: "Please enter an email.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
            
        } else {
            AppManager.instance.apiHandler.resetPassword(email: email, completionHandler: { (responseCode) in
                if responseCode!.getType() == "error" {
                    
                    
                    let alertController = UIAlertController(title: "Error", message: responseCode!.getMessage(), preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    DispatchQueue.main.async {
                        self.present(alertController, animated: true, completion: nil)
                    }
                } else {
                    
                    let alertController = UIAlertController(title: "Success", message: responseCode!.getMessage(), preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: { (action: UIAlertAction!) in
                        self.dismiss(animated: true, completion: nil)
                    })
                    alertController.addAction(defaultAction)
                    DispatchQueue.main.async {
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            })
        }
    }
}
