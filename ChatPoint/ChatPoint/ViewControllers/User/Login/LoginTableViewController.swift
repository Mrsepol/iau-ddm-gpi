//
//  LoginTableViewController.swift
//  ChatPoint
//
//  Created by Dany Mota on 27/12/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit
import Firebase
import FacebookLogin
import FBSDKLoginKit

class LoginTableViewController: UITableViewController, LoginButtonDelegate, UITextFieldDelegate {

    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var emailInput: UITextField!{
        didSet {
            emailInput.setIcon(UIImage(named: "icon_email")!)
        }
    }
    @IBOutlet weak var passwordInput: UITextField!{
        didSet {
            passwordInput.setIcon(UIImage(named: "icon_password")!)
        }
    }
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var createAccount: UIButton!
    
    let facebookLoginButton = LoginButton(readPermissions: [ .publicProfile ])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        facebookLoginButton.center = CGPoint.init(x: super.view.center.x, y: ((self.view.center.y/8 * 7) * 2) - 30 < 514 ? 514 : (self.view.center.y/8 * 7) * 2 - 30)
        
        self.hideKeyboardWhenTappedAround()
        emailInput.delegate = self
        passwordInput.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Shadow and Radius Login Button
        loginButton.layer.shadowColor = UIColor.black.cgColor
        loginButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        loginButton.layer.shadowRadius = 2
        loginButton.layer.shadowOpacity = 1.0
        loginButton.layer.cornerRadius=3
        loginButton.isEnabled = AppManager.instance.hasConnection
        createAccount.isEnabled = AppManager.instance.hasConnection
        forgotPasswordButton.isEnabled = AppManager.instance.hasConnection
        // Shadow and Radius FaceBook Button
        facebookLoginButton.layer.shadowColor = UIColor.black.cgColor
        facebookLoginButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        facebookLoginButton.layer.shadowRadius = 2
        facebookLoginButton.layer.shadowOpacity = 1.0
        facebookLoginButton.layer.cornerRadius = 3
        facebookLoginButton.delegate = self as LoginButtonDelegate
        
        view.addSubview(facebookLoginButton)
        
        if (!AppManager.instance.hasConnection) {
            AppManager.instance.alertManager.showErrorAlert(controller: self, "No internet connection detected", title: "Network error", onOk: nil)
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        
        if(UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight){
            facebookLoginButton.center = CGPoint.init(x: super.view.center.x, y: 514)
        }else if(UIDevice.current.orientation == .portrait){
           facebookLoginButton.center = CGPoint.init(x: super.view.center.x, y: ((self.view.center.y/8 * 7) * 2) - 30 < 514 ? 514 : (self.view.center.y/8 * 7) * 2 - 30)
        }
    }
    
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        
        switch result {
        case .failed(let error):
            print(error)
            break
        case .cancelled:
            break
        case .success( _, _, _):
            AppManager.instance.apiHandler.loginWithFacebook() { (authResult) in
                if authResult!.getType() == "success" {
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: nil)
                    }
                }else {
                    if FBSDKAccessToken.currentAccessTokenIsActive(){
                        let loginManager = FBSDKLoginManager()
                        loginManager.logOut()
                    }
                    let alertController = UIAlertController(title: "Error", message: authResult!.getMessage(), preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    DispatchQueue.main.async {
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                    return
                }
                
                
            }
            break
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: LoginButton) {
        //
        AppManager.instance.apiHandler.signOut() {
            (responseCode) in
            if responseCode!.getType() == "success" {
                
                print("Signout successfully")
                return
            }
            //Tells the user that there is an error and then gets firebase to tell them the error
            let alertController = UIAlertController(title: "Error", message: responseCode!.getMessage(), preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            DispatchQueue.main.async {
                self.present(alertController, animated: true, completion: nil)
            }
            return
            
        }
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailInput {
            passwordInput.becomeFirstResponder()
            return false
        }
        
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    
    @IBAction func onClickLogin(_ sender: Any) {
        
        let email = self.emailInput.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = self.passwordInput.text
        
        if email == "" || password == "" {
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Please enter an email and password.", title: "Error", onOk: nil)
        } else if !AppManager.instance.inputManager.validateLengthString(string: password, n: 8){
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Please enter a valid password. \n At least 8 characters!", title: "Error", onOk: nil)
        } else {
            AppManager.instance.alertManager.showLoadingAlert(controller: self, "")
            AppManager.instance.apiHandler.login(email: email!, password: password!) { (responseCode)  in
                AppManager.instance.alertManager.hiddenLoadingAlert(controller: self, completion: nil)
                if responseCode!.getType() == "success" {
                    //Print into the console if successfully logged in
                    print(responseCode!.getMessage())
                    
                    //Go to the HomeViewController if the login is sucessful
                    self.dismiss(animated: true, completion: nil)
                    
                }
                else {
                    
                    AppManager.instance.alertManager.showErrorAlert(controller: self, responseCode!.getMessage(), title: "Error", onOk:nil)
                }
            }
        }
    }
    
    @IBAction func onClickForgotPassword(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPassword")
        self.present(vc!, animated: true, completion: nil)
        
    }
    
    @IBAction func goToAccountRegistation(_ sender: Any) {
        if let registerView = self.storyboard?.instantiateViewController(withIdentifier: "registerController") {
            self.present(registerView, animated: true, completion: nil)
        }
    }
   
}

//    Extention to use icon on input
extension UITextField {
    
    func setIcon(_ image: UIImage) {
        let iconView = UIImageView(frame:
            CGRect(x: 10, y: 5, width: 20, height: 20))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame:
            CGRect(x: 20, y: 0, width: 30, height: 30))
        iconContainerView.addSubview(iconView)
        leftView = iconContainerView
        leftViewMode = .always
    }
    
}
