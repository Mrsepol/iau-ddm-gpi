//
//  TabBarViewController.swift
//  ChatPoint
//
//  Created by João Pereira Marques on 17/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit
import CoreLocation
class TabBarViewController: UITabBarController, UITabBarControllerDelegate {

    var isFromURL: Bool = false
    var chatID: String = ""
    var currentLocation: CLLocationCoordinate2D?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isFromURL {
            // cleanup
            isFromURL = false
            if let navToNearbyChats = self.children[0] as? UINavigationController {
                if let mapV = navToNearbyChats.children[0] as? NearbyChatsViewController {
                    mapV.enterIntoChat = true
                    mapV.currentChat = AppManager.instance.chatManager.getNearbyChatByID(id: chatID)
                    mapV.currentLocation = self.currentLocation
                }
            }
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        if (!AppManager.instance.hasConnection && viewController.isKind(of: CreateChatNavigationViewController.self)) {
            AppManager.instance.alertManager.showErrorAlert(controller: self, "Can't create a new chat without internet connection", title: "Network error", onOk: nil)
            return false
        }
        
        if AppManager.instance.getUser() == nil &&
            (viewController.isKind(of: MyAccountNavigationViewController.self) ||
                viewController.isKind(of: MyChatNavigationViewController.self) ||
                viewController.isKind(of: CreateChatNavigationViewController.self)
            ) {
            if let loginController = self.storyboard?.instantiateViewController(
                withIdentifier: "Login") {
                self.present(loginController, animated: false, completion: nil)
            }
            return false
        }
        return true
    }
}
