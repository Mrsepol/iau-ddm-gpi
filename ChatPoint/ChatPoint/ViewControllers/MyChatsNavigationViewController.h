//
//  MyChatsNavigationViewController.h
//  ChatPoint
//
//  Created by Joao Marques on 20/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyChatsNavigationViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
