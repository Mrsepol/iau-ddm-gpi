//
//  Created by Dany Mota on 08/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class MyChatListTableViewController: UITableViewController {
    
    // TODO: Tirar isto porque esta mal
    //var chatsRef = [String]()
    //var chatsID = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*
        let db = Firestore.firestore()
        
        db.collection("chats").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    if let name = document.data()["name"] {
                        self.chatsRef.append(name as! String)//document.documentID)
                        self.chatsID.append(document.documentID)
                    }
                }
                self.tableView.reloadData()
            }
        }
        */
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppManager.instance.chatManager.getUserChats { (responseCode) in
            if (responseCode?.getType() == "success") {
                self.tableView.reloadData()
            } else {
                print("Erro ao obter chats")
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return AppManager.instance.chatManager.myChats.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatCell", for: indexPath) as! MyChatListTableViewCell
        
        // Configure the cell...
        cell.chatID = AppManager.instance.chatManager.myChats[indexPath.row].id
        cell.textLabel?.text = AppManager.instance.chatManager.myChats[indexPath.row].name

        return cell
    }
    

    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
        let selectedCell = sender as! MyChatListTableViewCell
        
        if let vc = segue.destination as? ChatViewController
        {
            vc.currentChatID = selectedCell.chatID
        }
     }
 
    
}
