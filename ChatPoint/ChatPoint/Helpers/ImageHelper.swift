//
//  ImageDecoder.swift
//  ChatPoint
//
//  Created by formando on 14/11/2018.
//  Copyright © 2018 ChatPoint. All rights reserved.
//

import Foundation
import UIKit

class ImageHelper {
    static func getBase64FromImage(image:UIImage) -> String  {
        return image.pngData()!.base64EncodedString()
    }
    static func getImageFromBase64String(string:String) throws -> UIImage {
        
        guard let data = Data(base64Encoded: string, options: .ignoreUnknownCharacters) else {
            throw ErrorCodes.invalidbase64Image
        }
        return UIImage(data: data)!
    }
   
    static func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
